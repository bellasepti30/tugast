package Person;

public class Segitiga {

    int Alas;
    int Tinggi;
    Segitiga (int A, int T)
    {
        Alas = A;
        Tinggi = T;
    }
    double Luas() {
        return (Alas * Tinggi / 2);
    }

    public static void main(String[] args) {
        Segitiga segi_siku = new Segitiga(20,45);
        System.out.printf("Alas? %d%s\n", segi_siku.Alas , " cm");
        System.out.printf("Tinggi? %d%s\n", segi_siku.Tinggi, " cm");
        System.out.printf("Hasil? %s", segi_siku.Luas());
    }
}