package Person;

public class MataKuliah {
    String kodeMK; // ini variabel global
    String namaMK;

    MataKuliah(String namaMK, String kodeMK){ // constructor berparameter
        this.namaMK=namaMK;
        this.kodeMK=kodeMK;
    }
    // contructor tidak berparameter
//    MataKuliah(){
//    }

    // apakah java tidak ada constructor? ada seperti di atas sudah tertulis tanpa kita tahu
    // hingga constructoe nya berparameter

    void tambahMK(String kodeMK, String namaMK){
       this.kodeMK= kodeMK; //variabel global = bisa diakses dari semua kalangan fungsi/method
       this.namaMK= namaMK;
    }

    void cetakMK() {
        System.out.println("Data Mata Kuliah");
        System.out.println("Kode Mata Kuliah : "+kodeMK);
        System.out.println("Nama Mata Kuliah : "+namaMK);
    }

    //variabel lokal = tdak bisa diakses method lain
    //karena tidak terdeteksi di variabel global bahkan dipanggil di class sendiri
    //jadi variabel lokal ga akan terusik di global
    void inputData(String namaMK, String kodeMK){ // parameter itu dinamakan variabel lokal
        this.namaMK=namaMK; //this ibarat proses pengiriman
        this.kodeMK=kodeMK;
    }
    //clean code = meminimalisir code, di parameter dengan di body bisa aja beda variabel
    // -tapi diusahakan sama
}

//tugas bikin diagram
//atribut tipedata : variabel
//method namaMethod() : void/tipedata nilai