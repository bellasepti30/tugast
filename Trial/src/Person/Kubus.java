package Person;

import java.util.Scanner;

public class Kubus {
    // method void kubus
    public void nilaiVolume (double sisi, String kubik){
        int volume = (int) Math.pow(sisi, 3);
        System.out.printf("-Volume Kubus adalah %s %s\n" ,volume, kubik);
    }
    public void nilaiLuasPerm (double sisi, String kuadrat){
        int luasPerm =  (int) (6 * Math.pow(sisi, 2));
        System.out.printf("-Luas Permukaan Kubus adalah %s %s\n",luasPerm, kuadrat);
    }
    public void nilaiDiagonalRuang (double sisi, String sentim){
        double diagonalRuang = Math.sqrt(3) * sisi;
        System.out.printf("-Diagonal Ruang Kubus adalah %s %s\n",diagonalRuang, sentim);
        }
    public void nilaiPanjangRusuk_dariVolume (double sisi, String sentim){
        int volume = (int) Math.pow(sisi, 3);
        int panjangRusuk = (int) Math.cbrt(volume);
        System.out.printf("-Panjang Rusuk atau Sisi dari Volume %s adalah %s %s\n", volume ,panjangRusuk, sentim);
        }
    public void nilaiPanjangRusuk_dariLuasPerm (double sisi, String sentim){
        int luasPerm = (int) (6 * Math.pow(sisi, 2));
        int panjangRusuk = (int) Math.sqrt(luasPerm/6);
        System.out.printf("-Panjang Rusuk atau Sisi dari Luas Permukaan %s adalah %s %s\n", luasPerm ,panjangRusuk, sentim);
        System.out.println();
    }

    // method return value bangun datar 4 sisi
    public static double luasPersegi (double sisi){
         return Math.pow(sisi,2);
    }
    public static double kelilingPersegi (double sisi){
        return 4 * sisi;
    }
    public static double luasJajarGenjang (double alas, double tinggi){
        return alas * tinggi;
    }
    public static double kelilingJajarGenjang (double alas, double samping){
        return 2 * (alas + samping);
    }
    public static double luasPersegiPanjang (double alas, double tinggi){
        return alas * tinggi;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("--Rumus Kubus--");
        String sentim = "cm";
        String kuadrat = "cm^2";
        String kubik = "cm^3";
        System.out.print("Input Sisi = ");
        double sisi = input.nextDouble();
        System.out.printf("             %s %s\n", sisi, sentim);
        System.out.println();

        Kubus kubus_ku = new Kubus();
        kubus_ku.nilaiVolume(sisi,kubik);
        kubus_ku.nilaiLuasPerm(sisi, kuadrat);
        kubus_ku.nilaiDiagonalRuang(sisi, sentim);
        kubus_ku.nilaiPanjangRusuk_dariVolume(sisi, sentim);
        kubus_ku.nilaiPanjangRusuk_dariLuasPerm(sisi, sentim);

        System.out.println("--Rumus Bangur Datar 4 Sisi--");
        System.out.print("Input Alas = ");
        double alas = input.nextDouble();
        System.out.printf("             %s cm\n", alas);
        System.out.println();
        System.out.print("Input Tinggi = ");
        double tinggi = input.nextDouble();
        System.out.printf("               %s cm\n", tinggi);
        System.out.println();
        System.out.print("Input Samping = ");
        double samping = input.nextInt();
        System.out.printf("                %s cm\n", samping);
        System.out.println();

        int luasPersegi = (int) luasPersegi(sisi);
        System.out.println("-Luas dari Persegi Adalah = " + luasPersegi +" "+ kuadrat);

        int kelilingPersegi =  (int) kelilingPersegi(sisi);
        System.out.println("-Keliling dari Persegi Adalah = " + kelilingPersegi+" "+kuadrat);

        int luasJajarGenjang =  (int) luasJajarGenjang(alas, tinggi);
        System.out.println("-Luas dari Jajar Genjang Adalah = " + luasJajarGenjang+" "+kuadrat);

        int kelilingJajarGenjang =  (int) kelilingJajarGenjang(alas, samping);
        System.out.println("-Keliling dari Jajar Genjang Adalah = " + kelilingJajarGenjang+" "+kuadrat);

        int luasPersegiPanjang =  (int) luasPersegiPanjang(alas, tinggi);
        System.out.println("-Luas dari Persegi Panjang Adalah = " + luasPersegiPanjang+" "+kuadrat);
    }
    }

