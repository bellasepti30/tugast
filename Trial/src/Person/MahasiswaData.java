package Person;

//public class MahasiswaData
//{
//    public static void main(String[] args)
//    {
//        Mahasiswa mhs_ti = new Mahasiswa();
//        mhs_ti.nama = "Arkan";
//        mhs_ti.nim= "18183:";
//        mhs_ti.kelas = "Tiga B2";
//        mhs_ti.umur = 8;
//
//        System.out.println("--Data Mahasiswa--");
//        System.out.printf("Nama    : %s\n", mhs_ti.nama);
//        System.out.printf("NIM     : %s\n", mhs_ti.nim);
//        System.out.printf("Prodi   : %s\n", mhs_ti.kelas);
//        System.out.printf("Umur    : %d\n", mhs_ti.umur);
//    }
//}

public class MahasiswaData
{
    public static void main(String[] args)
    {
        Mahasiswa mhs_ti = new Mahasiswa("Bella", "1661", "TI A", 20,'5');

        System.out.println("--Data Mahasiswa--");
        mhs_ti.cetakData();
    }
}