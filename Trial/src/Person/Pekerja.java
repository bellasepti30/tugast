package Person;

public class Pekerja {
    String nama;
    int usia;
    String bidang;
    double gaji;
    String lokasi;

    Pekerja (String nama, int usia, String bidang, double gaji, String lokasi){
        this.nama=nama;
        this.usia=usia;
        this.bidang=bidang;
        this.gaji=gaji;
        this.lokasi=lokasi;
    }

   void cetakPekerja(){
       System.out.println("Jenis Pekerjaan dengan Ragam Bidang");
       System.out.println("Informasi Pekerja:");
       System.out.printf("Nama      : %s\n", nama);
       System.out.printf("Usia      : %s\n", usia);
       System.out.printf("Bidang    : %s\n", bidang);
       System.out.printf("Gaji      : Rp. %s / bulan\n", gaji);
       System.out.printf("Lokasi    : %s\n", lokasi);
   }

    public static void main(String[] args) {
        Pekerja kesehatan = new Pekerja("Profesional Keperawatan", 29, "Kesehatan", 4_000_000, "Klinik Westerindo Bandung");
        kesehatan.cetakPekerja();

        Pekerja hukum = new Pekerja("Pengacara Junior", 32, "Hukum", 25_000_000, "Kantor Hukum Kota Jakarta");
        hukum.cetakPekerja();

        Pekerja transportasi = new Pekerja("Captain Pilot", 45, "Transportasi Udara", 50_000_000, "Maskapai Garuda Indonesia");
        transportasi.cetakPekerja();

        Pekerja perusahaan = new Pekerja("Manajer Keuangan", 40, "Perusahaan", 25_000_000, "Perusahaan Model Jakarta Pusat");
        perusahaan.cetakPekerja();

        Pekerja pendidikan = new Pekerja("Guru Matematika", 28, "Pendidikan", 4_000_000, "SMP Negeri 1 Surabaya");
        pendidikan.cetakPekerja();
    }
}
