package com.handphone;

public class HP
{
    private boolean hidup;
    private double jumlah_pulsa;
    final private String versi;
    HP (String ver , double pulsa){
        jumlah_pulsa = pulsa;
        versi = ver;
    }

    void hidupkan(){
        hidup = true;
        System.out.println("-handphone daya hidup-");
    }

    void panggil(String nomor){
        if (hidup){
            System.out.println(versi + "Sedang hubungi "+ nomor+ " ...");
            System.out.println("-Panggilan selesai-\n");
        }
    }

    void kirimSMS(String pesan){
        if (hidup && jumlah_pulsa>0){
            System.out.printf("Kirim SMS : %s\n" , pesan);
            System.out.println("-teng teng, SMS sudah terkirim-\n");
            jumlah_pulsa -= 0.1;
        }
    }

    public String merknya(){
        return versi;
    }

    public double pulsanya(){
        return jumlah_pulsa;
    }

    void matikan(){
        hidup = false;
        System.out.println();
        System.out.println("-handphone daya mati-");
        if (!hidup){
            System.out.println("Tolong nyalakan hp mu dahulu");
            System.out.println("Tidak dapat mengirim SMS. Isi pulsa Anda jika habis");
        }

    }

//    void isiPulsa(){
//        double jumlah =
//    }
}
