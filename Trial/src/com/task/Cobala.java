package com.task;
import java.util.Scanner;
public class Cobala
{
    public static void main (String args[])
    {
        /*masukan input=new masukan(System.in);

        System.out.print("Masukan Jumlah Kata : ");
        int n=input.nextInt();

        //Deklarasi array
        String kata[]=new String[n];

        //Input data ke array
        for(int i=0;i<kata.length;i++)
        {
            System.out.print("Kata ke "+(i+1)+" : ");
            kata [i]=input.next();
        }
        //Menampilkan data dalam array
        System.out.println("Kata-kata yang dimasukan");
        for(int i=0;i<kata.length;i++)
        {
            System.out.println(kata[i]);
           // System.out.println();
        }*/

                Scanner masukan = new Scanner(System.in);

                // Membaca ukuran matriks pertama
                int n_satu = masukan.nextInt();
                int n_dua = masukan.nextInt();

                // Membaca elemen-elemen matriks pertama
                int[][] mat_satu = new int[n_satu][n_dua];
                for (int i = 0; i < n_satu; i++) {
                    for (int j = 0; j < n_dua; j++) {
                        mat_satu[i][j] = masukan.nextInt();
                    }
                }

                // Membaca ukuran matriks kedua
                int n2 = masukan.nextInt();
                int m2 = masukan.nextInt();

                // Membaca elemen-elemen matriks kedua
                int[][] mat_dua = new int[n2][m2];
                for (int i = 0; i < n2; i++) {
                    for (int j = 0; j < m2; j++) {
                        mat_dua[i][j] = masukan.nextInt();
                    }
                }

                // Menentukan jenis refleksi
                String hasil = refleksional(mat_satu, mat_dua);

                // Menampilkan output
                System.out.println(hasil);
            }

            // Fungsi untuk menentukan jenis refleksi
            private static String refleksional(int[][] mat_satu, int[][] mat_dua) {
                if  (cekIdentik(mat_satu, mat_dua)) {
                    return "identik";
                } else if (vertikalRefleksiCek(mat_satu, mat_dua)) {
                    return "vertikal";
                } else if (horizontalRefleksiCek(mat_satu, mat_dua)) {
                    return "horizontal";
                } else if (diagonalRightDownRefleksi(mat_satu, mat_dua)) {
                    return "diagonal kanan bawah";
                } else if (diagonalLeftDownRefleksi(mat_satu, mat_dua)) {
                    return "diagonal kiri bawah";
                } else {
                    return "tidak identik";
                }
            }

            // Fungsi untuk memeriksa apakah dua matriks identik
            private static boolean cekIdentik(int[][] mat_satu, int[][] mat_dua) {
                int n = mat_satu.length;
                int m = mat_satu[0].length;

                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        if (mat_satu[i][j] != mat_dua[i][j]) {
                            return false;
                        }
                    }
                }

                return true;
            }

            // Fungsi untuk memeriksa apakah dua matriks identik secara vertikal
            private static boolean vertikalRefleksiCek(int[][] mat_satu, int[][] mat_dua) {
                int n = mat_satu.length;
                int m = mat_satu[0].length;

                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        if (mat_satu[i][j] != mat_dua[n - 1 - i][j]) {
                            return false;
                        }
                    }
                }

                return true;
            }

            // Fungsi untuk memeriksa apakah dua matriks identik secara horizontal
            private static boolean horizontalRefleksiCek(int[][] mat_satu, int[][] mat_dua) {
                int n = mat_satu.length;
                int m = mat_satu[0].length;

                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        if (mat_satu[i][j] != mat_dua[i][m - 1 - j]) {
                            return false;
                        }
                    }
                }

                return true;
            }

            // Fungsi untuk memeriksa apakah dua matriks identik secara diagonal kanan bawah (\)
            private static boolean diagonalRightDownRefleksi(int[][] mat_satu, int[][] mat_dua) {
                int n = mat_satu.length;
                int m = mat_satu[0].length;

                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        if (mat_satu[i][j] != mat_dua[j][i]) {
                            return false;
                        }
                    }
                }

                return true;
            }

            // Fungsi untuk memeriksa apakah dua matriks identik secara diagonal kiri bawah (/)
            private static boolean diagonalLeftDownRefleksi(int[][] mat_satu, int[][] mat_dua) {
                int n = mat_satu.length;
                int m = mat_satu[0].length;

                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        if (mat_satu[i][j] != mat_dua[m - 1 - j][n - 1 - i]) {
                            return false;
                        }
                    }
                }

                return true;
            }
        }