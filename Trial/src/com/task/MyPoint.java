package com.task;
//10.12
//235150707111002 (TI-A)
//Nabillah Septianisa Nur Azizah
public class MyPoint {
    private double x;
    private double y;

    //constructor with argument
    public MyPoint(double x, double y){
        //specified coordinate
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    //calculate the distance to a specified point (saat ini dan diberikan)
    public double distance(MyPoint point){
        return Math.sqrt(Math.pow(x - point.getX(), 2) + Math.pow(y - point.getY(), 2));
    }

    public static void main(String[] args) {
        //Create a Triangle2D object (t1)
        Triangle2D t1 = new Triangle2D(new MyPoint(2.5, 2), new MyPoint(4.2, 3), new MyPoint(5, 3.5));

        //Set new coordinates for p1, p2, p3 (updated)
        //memungkinkan bisa mengubah koordinat titik segitiga setelah objekk segitiga dibuah
//        t1.setP1(new MyPoint(3,2));
//        t1.setP2(new MyPoint(4,4));
//        t1.setP3(new MyPoint(6,5));

        System.out.println("Area : "+t1.getArea());
        System.out.println("Perimeter : "+t1.getPerimeter());
        System.out.println("Contains (3, 3) : "+t1.contains(new MyPoint(3, 3)));
        System.out.println("Contains new Triangle2D : "+t1.contains(new Triangle2D(new MyPoint(2.9, 2), new MyPoint(4, 1), new
                MyPoint(1, 3.4))));
        System.out.println("Overlaps new Triangle2D : "+t1.overlaps(new Triangle2D(new MyPoint(2, 5.5), new MyPoint(4, -3), new
                MyPoint(2, 6.5))));
    }
}


