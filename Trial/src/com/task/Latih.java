package com.task;
import java.util.Scanner;

public class Latih {

        public static void main(String[] args) {
            Scanner masuk = new Scanner(System.in);

            // Membaca input username dan password
            String username = masuk.nextLine();
            String password = masuk.nextLine();

            // Memeriksa validitas username
            String username_hasiln = hasilnUser(username);

            // Memeriksa validitas password
            String password_hasiln = hasilnPass(password);

            // Menampilkan hasil validasi
            System.out.println(username_hasiln);
            System.out.println(password_hasiln);
        }

        // Fungsi untuk memeriksa kalimat username mengandung spasi
        private static boolean cetakSpasi (String username){
            for (int i = 0; i < username.length(); i++){
                if (username.charAt(i) == ' '){
                    return true;
                }
            }
            return false;
        }

        // Fungsi untuk memeriksa validitas username yang dimasukkan
        private static String hasilnUser(String username) {
            String hasiln = "";

            // Memeriksa panjang username
            if (username.length() < 5) {
                hasiln+="Username tidak valid:\nPanjang username harus minimal 5 karakter.\n";
            }

            // Memeriksa apakah username mengandung spasi
            if (cetakSpasi(username)) {
                hasiln+="Username tidak valid:\nUsername tidak boleh mengandung spasi.\n";
            }

            // Memeriksa apakah hasil validasi kosong (username valid)
            if (hasiln.isEmpty()) {
                hasiln+="Username valid.\n";
            }

            return hasiln;
        }

        // Fungsi untuk memeriksa validitas password yang dimasukkan
        private static String hasilnPass(String password) {
            String hasiln = "";

            // Memeriksa panjang password
            if (password.length() < 8) {
                hasiln+="Password tidak valid:\nPanjang password harus minimal 8 karakter.\n";
            }

            // Cek password diawali dengan huruf kapital
            if (!Character.isUpperCase(password.charAt(0))) {
                hasiln+="Password harus diawali dengan huruf kapital.\n";
            }

            // Cek hasil validasi kosong (password valid)
            if (hasiln.isEmpty()) {
                hasiln+="Password valid.\n";
            }

            return hasiln;
        }
    }
