package com.task;
import java.util.Scanner;

//Nabillah Septianisa Nur Azizah
//235150707111002
//The intersecting point can be found by solving a linear equation.
//Write a program that prompts the user to enter these four endpoints and displays the intersecting point.

    public class LineIntersection {
        public static void main(String[] args) {
            Scanner input = new Scanner(System.in);

            System.out.println("Enter the endpoints of the first line segment (x1, y1, x2, y2): ");
            double x1 = input.nextDouble();
            double y1 = input.nextDouble();
            double x2 = input.nextDouble();
            double y2 = input.nextDouble();

            System.out.println("Enter the endpoints of the second line segment (x3, y3, x4, y4): ");
            double x3 = input.nextDouble();
            double y3 = input.nextDouble();
            double x4 = input.nextDouble();
            double y4 = input.nextDouble();

            //Objects
            LinearEquation eq = new LinearEquation(y1 - y2, x2 - x1, y3 - y4, x4 - x3, (y1 - y2) * x1 -
                                                                 (x1 - x2) * y1, (y3 - y4) * x3 - (x3 - x4) * y3);
            //Display the result
            if (eq.isSolvable()) {
                System.out.println("The intersecting point is at (" + eq.getX() + ", " + eq.getY() + ")");
            } else {
                System.out.println("The two lines are parallel and do not intersect.");
            }

            input.close();
        }
    }
//kalo ada method yg namanya sama dengan nama class dinamakan konstruktor
// konstruktor hanya bisa dipanggil dgn perintah new dan saat object pertama kali dibuat
//konstruktor Scanner (InputStream) = itu Scanner (System.in) dan ragam macam lainnya
//konstruktor bisa kosong tanpa assignment jadi di utama manggilnya juga tanpa nilai parameter
//kenapa class scanner ga bisa kosong parameter nya? karena sistem ga biarkan inputnya kosong
//nama method bisa sama dalam satu class tapi jumlah/tipe/keduanya parameter harus beda dinamakan overloading
// public Car (int speed, int power){
//          kecepatan = speed
//          tenaga = power}
// public Car (int speed, int power){
//        self.speed = speed
//        self.power = power}
//public static Car() atau public void Car() = method

//program stopwatch
//set() itu buat ganti
//misal ga bisa ganti ID bank atau nimm, itu bisanya baru
//jadi pake get()
//saldo sistemnya diproses ga bole setting sendiri jadi bernilai pakai method void
//otomatis tanggal secara sistem di awal pakai java date / new

//harusnya konstruktornya ada class x1,y1 trs class x2,y2, dsb
//harusnya juga tinggal input garis pertama dan garis kedu akemudian cek apakah berpotongan
//jadi rumus yg g ada pecahannya itu dimodif jadi ada pecahanny
