package com.task;

public class LinearEquation {


            private double a, b, c, d, e, f;

        public LinearEquation( double a, double b, double c, double d, double e, double f){
                this.a = a;
                this.b = b;
                this.c = c;
                this.d = d;
                this.e = e;
                this.f = f;
            }

            public boolean isSolvable () {
                return (a * d - b * c) != 0;
            }
            public double getX () {
                return (e * d - b * f) / (a * d - b * c);
            }
            public double getY () {
                return (a * f - e * c) / (a * d - b * c);
            }
//    public LinearEquation(double v, double v1, double v2, double v3, double v4, double v5) {
//            LinearEquation eq = new LinearEquation();
//            eq.inputGaris1(2, 2, 5, -1);
//            eq.inputGaris2();
//            eq.hitung
//
//            if (eq.isSolved) {
//                System.out.println(eq.getTitikPotongX);
//                System.out.println(eq.getTitikPotongY);
//
//            } else {
//            }
        }