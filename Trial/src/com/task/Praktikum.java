package com.task;

import java.util.Scanner;

public class Praktikum {
    public static void main(String[] args) {
        Scanner c = new Scanner(System.in);

        System.out.print("Masukkan bilangan pertama: ");
        int bil_1 = c.nextInt();

        System.out.print("Masukkan bilangan kedua: ");
        int bil_2= c.nextInt();

        System.out.println("Masukkan bilangan jumlah barisan yang ingin ditampilkan: ");
        int jumlahBarisan = c.nextInt();


        if (bil_1 < 0 || bil_2 < 0 || jumlahBarisan <= 0) {

        } else {

            // Menampilkan barisan Fibonacci dan menghitung jumlahnya
            System.out.println("Barisan Fibonacci:");

            //System.out.print(bilYangKe1 + ", ");
            //System.out.print(bilYangKe2 + ", ");


           if (jumlahBarisan == 1) {
                System.out.print(bil_1);
            }
            else if (jumlahBarisan == 2) {
                System.out.print(bil_2 + ", ");
            }
            int next_1 = bil_1 + bil_2;
            if (jumlahBarisan == 3) {
                System.out.print(bil_1 + ", " + bil_2 + ", " + next_1);
            }
            int next_2 = bil_2 + next_1;
            if (jumlahBarisan == 4) {
                System.out.print(bil_1 + ", " +bil_2 + ", " + next_1 + ", " + next_2);
            }
            int next_3 = next_1 + next_2;
            if (jumlahBarisan == 5) {
                System.out.print(bil_1 + ", " + bil_2 + ", " + next_1 + ", " + next_2 + ", " + next_3);
            }
            int next_4 = next_2 + next_3;
            if (jumlahBarisan == 6) {
                System.out.print(bil_1 + ", " + bil_2 + ", " + next_1 + ", " + next_2 + ", " + next_3 + ", " + next_4);
            }
            int next_5 =  next_3 + next_4;
            if (jumlahBarisan == 7) {
                 System.out.print(bil_1 + ", " + bil_2 + ", " + next_1 + ", " + next_2 + ", " + next_3 + ", " + next_4 + ", " + next_5);
            }
            int next_6 = next_4 + next_5;
            if (jumlahBarisan == 8) {
                System.out.print(bil_1 + ", " + bil_2 + ", " + next_1 + ", " + next_2 + ", " + next_3 + ", " + next_4 + ", " + next_5 + ", " + next_6);
            }
            int next_7 = next_5 + next_6;
            if (jumlahBarisan == 9) {
                System.out.print(bil_1 + ", " + bil_2 + ", " + next_1 + ", " + next_2 + ", " + next_3 + ", " + next_4 + ", " + next_5 + ", " + next_6 + ", " + next_7);
            }
            int next_8 = next_6 + next_7;
            if (jumlahBarisan == 10) {
                System.out.print(bil_1 + ", " + bil_2 + ", " + next_1 + ", " + next_2 + ", " + next_3 + ", " + next_4 + ", " + next_5 + ", " + next_6 + ", " + next_7 + ", " + next_8);
            }
            int next_9 = next_7 + next_8;
            if (jumlahBarisan == 11) {
                System.out.print(bil_1 + ", " + bil_2 + ", " + next_1 + ", " + next_2 + ", " + next_3 + ", " + next_4 + ", " + next_5 + ", " + next_6 + ", " + next_7 + ", " + next_8 + ", " + next_9);
            }

            }
        }
    }

