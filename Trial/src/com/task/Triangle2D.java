package com.task;
//10.12
//235150707111002 (TI-A)
//Nabillah Septianisa Nur Azizah
public class Triangle2D {
    private MyPoint p1;
    private MyPoint p2;
    private MyPoint p3;

    public Triangle2D() {
        //no-argument a default triangle
        this.p1 = new MyPoint(0, 0);
        this.p2 = new MyPoint(1, 1);
        this.p3 = new MyPoint(2, 5);
    }

    public Triangle2D(MyPoint p1, MyPoint p2, MyPoint p3) {
        //with specified points
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public MyPoint getP1() {
        return this.p1;
    }

    public void setP1(MyPoint p1) {this.p1 = p1;}

    public MyPoint getP2() {
        return this.p2;
    }

    public void setP2(MyPoint p2) {
        this.p2 = p2;
    }

    public MyPoint getP3() {
        return this.p3;
    }

    public void setP3(MyPoint p3) {
        this.p3 = p3;
    }

    //Calculate the area of the triangle with lengths (side)
    public double getArea() {
        //calculate the distance between points by each other
        double side1 = p1.distance(p2);
        double side2 = p2.distance(p3);
        double side3 = p3.distance(p1);

        double s = (side1 + side2 + side3) / 2;
        //Heron's formula
        return Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
    }

    //Calculate the perimeter by summing lengths (side)
    public double getPerimeter() {
        //calculate the distance between points by each other
        double side1 = p1.distance(p2);
        double side2 = p2.distance(p3);
        double side3 = p3.distance(p1);
        // the Math.sqrt and Math.pow functions are used to calculate the distances between the points.
        return side1 + side2 + side3;
    }

    //check if a point is inside the triangle
    public boolean contains(MyPoint point) {
        double areaOriginal = getArea();
        double area1 = new Triangle2D(point, p2, p3).getArea();
        double area2 = new Triangle2D(p1, point, p3).getArea();
        double area3 = new Triangle2D(p1, p2, point).getArea();

        return area1 + area2 + area3 == areaOriginal;
    }

    //check if another triangle overlaps with this triangle
    public boolean overlaps(Triangle2D t) {
        return !((!contains(t.getP1()) && !contains(t.getP2()) && !contains(t.getP3()))
                && (!t.contains(getP1()) && !t.contains(getP2()) && !t.contains(getP3())));
    }

    //check if another triangle is completely inside this triangle
    public boolean contains(Triangle2D t) {
        return contains(t.getP1()) && contains(t.getP2()) && contains(t.getP3());
    }
}
