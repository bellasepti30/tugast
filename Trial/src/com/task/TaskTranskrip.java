package com.task;

    public class TaskTranskrip {
        public static class Mahasiswa {
            String nama;
            int nim;
            String prodi;
            int tahunmasuk;
            Transkrip[] transkrip;
        }

        public static class Kursus {
            String nama;
            int kodematkul;
            int sks;
        }

        public static class Transkrip {
            Mahasiswa mhs;
            Kursus kursus;
            int nilai;
        }

            public static void addTranscript(Mahasiswa mahasiswa, Transkrip transkrip, Transkrip[] transkrips) {
                Transkrip[] transkripBaru = new Transkrip[transkrips.length + 1];
                for (int i = 0; i < transkrips.length; i++) {
                    transkripBaru[i] = transkrips[i];
                }
                transkripBaru[transkrips.length] = transkrip;
                mahasiswa.transkrip = transkripBaru;
            }

            public static void title(Mahasiswa mhs, Transkrip[] transkrips) {
                System.out.println("Transkrip nilai untuk " + mhs.nama + ":");
                for (Transkrip transcript : transkrips) {
                    if (transcript != null && transcript.mhs == mhs) {
                        cetakTranscript(transcript);
                    }
                }
            }

            public static void cetakTranscript(Transkrip transcript) {
                System.out.println(transcript.kursus.nama + ": " + transcript.nilai);
            }

            public static void main(String[] args) {
                Mahasiswa mhs1 = new Mahasiswa();
                //LENGKAPI CODE DISINI (5)
                mhs1.nama = "Aulia";
                mhs1.nim = 2351;
                mhs1.prodi = "TI";
                mhs1.tahunmasuk = 2023;
                mhs1.transkrip = new Transkrip[0];

                Mahasiswa mhs2 = new Mahasiswa();
                //LENGKAPI CODE DISINI (2.5)
                mhs2.nama = "Cindy";
                mhs2.nim = 2352;
                mhs2.prodi = "SI";
                mhs2.tahunmasuk = 2023;
                mhs2.transkrip = new Transkrip[1];

                Kursus mk1 = new Kursus();
                //LENGKAPI CODE DISINI (5)
                mk1.nama = "Dasar Desain Antarmuka Pengguna";
                mk1.kodematkul = 5150;
                mk1.sks = 4;

                Kursus mk2 = new Kursus();
                //LENGKAPI CODE DISINI (2.5)
                mk2.nama = "Matematika Komputasi";
                mk2.kodematkul = 6162;
                mk2.sks = 2;

                Transkrip[] transcripts = new Transkrip[4];

                Transkrip t1 = new Transkrip();
                //LENGKAPI CODE DISINI (7.5)
                t1.mhs = mhs2;
                t1.kursus = mk1;
                t1.nilai = 80;
                transcripts[0]=t1;

                Transkrip t2 = new Transkrip();
                //LENGKAPI CODE DISINI (2.5)
                t2.mhs = mhs2;
                t2.kursus = mk2;
                t2.nilai = 90;
                transcripts[1]=t2;

                Transkrip t3 = new Transkrip();
                //LENGKAPI CODE DISINI (2.5)
                t3.mhs = mhs1;
                t3.kursus = mk1;
                t3.nilai = 75;
                transcripts[2]=t3;

                Transkrip t4 = new Transkrip();
                //LENGKAPI CODE DISINI (2.5)
                t4.mhs = mhs1;
                t4.kursus = mk2;
                t4.nilai = 97;
                transcripts[3]=t4;

                /*LENGKAPI CODE DISINI (5)*/
                addTranscript(mhs2, t1, transcripts);
                /*LENGKAPI CODE DISINI (2)*/
                addTranscript(mhs2, t2, transcripts);
                /*LENGKAPI CODE DISINI (3)*/
                title(mhs2, transcripts);
                /*LENGKAPI CODE DISINI (5)*/
                addTranscript(mhs1, t3, transcripts);
                /*LENGKAPI CODE DISINI (2)*/
                addTranscript(mhs1, t4, transcripts);
                /*LENGKAPI CODE DISINI (3)*/
                title(mhs1, transcripts);
            }
        }
