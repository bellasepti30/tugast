package com.task;
//10.22
//235150707111002 (TI-A)
//Nabillah Septianisa Nur Azizah
import java.util.Arrays;

public class MyString1 { //dalam string/kalimat ada banyak character
    private final char[] chars; //relasi one to many

    //initialize the the object with a chars array
    public MyString1(char [] chars){
        this.chars = chars;
    }

    //get the character at a specified index
    public char charAt(int index){
     return chars[index];
    }

    //get value of the length of the string (MyString1)
    public int length(){
        return chars.length;
    }

    //display a substring of the string (MyString1)
    public MyString1 substring(int begin, int end){
    char[] objekChars = new char[end - begin];
    System.arraycopy(chars, begin, objekChars, 0, end - begin);
    return new MyString1(objekChars);
    }

    //convert to lower case
    public  MyString1 toLowerCase(){
    char[] lowChars = new char[chars.length];
    for (int i = 0; i < chars.length; i++){
        lowChars[i] = Character.toLowerCase(chars[i]);
    }
    return new MyString1(lowChars);
    }

    //check if two strings are equal or not
    public boolean equals(MyString1 other){
    if (other.length() != this.length()){
        return false;
    }
    for (int i = 0; i < chars.length; i++){
        if (chars[i] != other.charAt(i)){
            return false;
        }
    } return true;
    }

    //convert an integer to a string then array chars
    public static MyString1 valueOf(int i){
    return new MyString1(String.valueOf(i).toCharArray());
    }

    public String toString() {
        return new String(chars);
    }

    public static void main(String[] args) {
        char[] chars = {'H', 'E', 'l', 'l', 'o', 'W'};
        MyString1 myObject = new MyString1(chars);

        System.out.println("Original String: " + myObject);
        System.out.println("Character at index 1: " + myObject.charAt(1));
        System.out.println("Length of the string: " + myObject.length());
        System.out.println("Substring from index 3 to 5: " + myObject.substring(3, 6).toString());
        System.out.println("String in lowercase: " + myObject.toLowerCase().toString());
        System.out.println("Is it equal to 'hellow'? " + myObject.equals(new MyString1("hellow".toCharArray())));
        System.out.println("String value Of(123): " + Arrays.toString(valueOf(123).chars));
    }
}
