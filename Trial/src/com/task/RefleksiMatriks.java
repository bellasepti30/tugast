import java.util.Scanner;

public class RefleksiMatriks {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n1 = scanner.nextInt();
        int m1 = scanner.nextInt();

        int[][] matriks1 = new int[n1][m1];
        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < m1; j++) {
                matriks1[i][j] = scanner.nextInt();
            }
        }

        int n2 = scanner.nextInt();
        int m2 = scanner.nextInt();

        int[][] matriks2 = new int[n2][m2];
        for (int i = 0; i < n2; i++) {
            for (int j = 0; j < m2; j++) {
                matriks2[i][j] = scanner.nextInt();
            }
        }

        String hasil = identifikasiRefleksi(matriks1, matriks2);

        System.out.println(hasil);

    }

    public static String identifikasiRefleksi(int[][] matriks1, int[][] matriks2) {
        if (apaMatriksSama(matriks1, matriks2)) {
            return "identik";
        } else if (apaMatriksSama(refleksiVertikal(matriks1), matriks2)) {
            return "vertikal";
        } else if (apaMatriksSama(refleksiHorizontal(matriks1), matriks2)) {
            return "horizontal";
        } else if (apaMatriksSama(refleksidiagonalkananBawah(matriks1), matriks2)) {
            return "diagonal kanan bawah";
        } else if (apaMatriksSama(refleksidiagonalkiriBawah(matriks1), matriks2)) {
            return "diagonal kiri bawah";
        } else {
            return "tidak identik";
        }
    }

    public static boolean apaMatriksSama(int[][] matriks1, int[][] matriks2) {
        for (int i = 0; i < matriks1.length; i++) {
            for (int j = 0; j < matriks1[0].length; j++) {
                if (matriks1[i][j] != matriks2[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static int[][] refleksiVertikal(int[][] matriks) {
        int[][] hasil = new int[matriks.length][matriks[0].length];
        for (int i = 0; i < matriks.length; i++) {
            for (int j = 0; j < matriks[0].length; j++) {
                hasil[i][j] = matriks[i][matriks[0].length - 1 - j];
            }
        }
        return hasil;
    }

    public static int[][] refleksiHorizontal(int[][] matriks) {
        int[][] hasil = new int[matriks.length][matriks[0].length];
        for (int i = 0; i < matriks.length; i++) {
            for (int j = 0; j < matriks[0].length; j++) {
                hasil[i][j] = matriks[matriks.length - 1 - i][j];
            }
        }
        return hasil;
    }

    public static int[][] refleksidiagonalkananBawah(int[][] matriks) {
        int[][] hasil = new int[matriks.length][matriks[0].length];
        for (int i = 0; i < matriks.length; i++) {
            for (int j = 0; j < matriks[0].length; j++) {
                hasil[i][j] = matriks[j][i];
            }
        }
        return hasil;
    }

    public static int[][] refleksidiagonalkiriBawah(int[][] matriks) {
        int[][] hasil = new int[matriks.length][matriks[0].length];
        for (int i = 0; i < matriks.length; i++) {
            for (int j = 0; j < matriks[0].length; j++) {
                hasil[i][j] = matriks[matriks.length - 1 - j][matriks[0].length - 1 - i];
            }
        }
        return hasil;
    }
}