package com.company;
import java.util.Scanner;
// Nama = Nabillah Septianisa Nur Azizah
// NIM = 235150707111002

public class Pemdas{

// 5.2 (Repeat additions)
    public static void main (String[] args){

                // Diawali method ini dengan mendeklarasikan sesuai tipe data masing-masing
                int banyakPertanyaan = 10; // sebanyak 1 - 10 soal pertanyaan penjumlahan
                int banyakKebenaran = 0; // hitung jumlah jawaban yang benar
                int perhitungan = 0; // soal dimulai dengan urutan ke-1
                long startTime = System.currentTimeMillis();
                Scanner input = new Scanner(System.in);

                System.out.println("\nAnswer This Addition Question!");

                    // Membuat 2 bilangan bulat dari 1-15 sampai batas 10 pertanyaan
                while (perhitungan < banyakPertanyaan) {
                    int n_1 = (int)(Math.random() * 15 + 1); // Menggunakan +1 agar 15 bisa terbaca
                    int n_2 = (int)(Math.random() * 15 + 1);
                    perhitungan++; // akan terus berjalan hingga mencapai batas

                     // Pengguna memasukkan jawaban dari pertanyaan penjumlahan
                     System.out.print("Q." + (perhitungan) + "\t\tWhat is " + n_1  + " + " + n_2 + " ? ");
                     int answer = input.nextInt();
                     int kebenaranNilai = n_1  + n_2;

                     // Membuktikan kebenaran antara jawaban input dari user sama atau tidak dengan jawaban penjumlahan
                     if (kebenaranNilai == answer) {
                         System.out.println("\t\tYou are correct the answer!\n");
                         banyakKebenaran++;// akan terus berjalan dan disimpan jumlahnya hingga mencapai batas
                         }

                     // JIka jawaban tidak sama maka menampilkan output berikut
                     else{
                     System.out.println("\t\tYour are incorrect the answer!\n" + "\t\t" + n_1
                             + " + " + n_2 + " should be " + (n_1 + n_2) + "\n");
                     }
                 }
                // Menghentikan durasi pengerjaan
                long endTime = System.currentTimeMillis();
                // Kemudian menghitung berapa lama durasi telah dilewati
                long testTime = endTime - startTime;

                //Menampilkan output durasi selama pengerjaan dalam konversi satuan dari miliseconds ke seconds
                System.out.println("\nCorrect count is " + banyakKebenaran +
                "\nTest time is " + testTime / 1000 + " seconds\n");
            }
    }

// sensitif besar kecil pengaruh

// insensitif ga ngaruh
 // cth : input MatIkan atau matikan sama aja