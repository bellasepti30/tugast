package com.company;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class Latihan {

    public static void main(String[] args) {
        // mencari berapa karakter
        /*String nama = "Fikri Noor Arafah"; // 0-16 urutan termasuk spasi
        String namaPanggil = nama.substring(0, 16);
        int substring = nama.length();
        // System.out.println(namaPanggil);
        System.out.println("kata nama berjumlah = " + substring);

        for (int i = 0, j = 6; j <= nama.length(); i++, j++) {
            System.out.println(nama.substring(i, j));
            while (nama.substring(i, j).equals("Fikri")) {
                break;
            }
            while (nama.substring(i, j).equals("Noor")) {
                break;
            }
            while (nama.substring(i, j).equals("Arafah")) {
                break;
            }
       /* double formatRupiah = 10_000; // float formatRupiah = 10_000f;
        System.out.printf("Rp %,.3f" , formatRupiah); // , untuk print koma trs .2f dua angka dibelakang koma 10.000
        // prntln atau print pakai tanda + untuk gabungan
        // printf pakai tanda , untuk gabungan */
        //  Scanner input = new Scanner(System.in);
     /*   int jumlahKata = 1;
        int i;
        String kata = input.nextLine();

        if (kata.isEmpty()) {
            jumlahKata = 0;
        }
         for (i = 0; i < kata.length(); i++) {
             if(kata.substring(i, (i+1)).equals(" ")){
                 jumlahKata++;
             }
        }
        System.out.println(jumlahKata);
        Scanner in = new Scanner(System.in);

        int rand = (int) (Math.random() * 100);
        System.out.println(rand);

        int i;

        for (i = 0; i < 5; i++ ){
            int tebakan = in.nextInt();
            if (tebakan == rand) {
                System.out.println("bener");
                break;

            } else if (tebakan > rand) {
                System.out.println("kelebihen");

            } else if (tebakan < rand) {
                System.out.println("kurangen");

            }


        }
    }
}*/
        Scanner input = new Scanner(System.in);

        //kode kalian mulai
        String nama = input.nextLine();

        double beratBarang = input.nextDouble();
        if (beratBarang <= 0 || beratBarang > 2000) {
            System.out.println("Berat tidak sesuai ketentuan!"); }

        input.nextLine();

        String tipeEkspedisi = input.nextLine().toLowerCase();
        String hari2 = input.nextLine();
        double hargaPengiriman = 0.0;
        int estimasi = 0;

        if (tipeEkspedisi.equalsIgnoreCase("regular")) {
            hargaPengiriman = 10000.0;
            estimasi = 5; }

        else if (tipeEkspedisi.equalsIgnoreCase("express")) {
            hargaPengiriman = 20000.0;
            estimasi = 3; }

        else if (tipeEkspedisi.equalsIgnoreCase("super")) {
            hargaPengiriman = 50000.0;
            estimasi = 1; }

        else {
            System.out.println("Pilihan ekspedisi tidak valid!"); }
        input.nextLine();

        if (beratBarang > 100) {
            hargaPengiriman += 30000;
            estimasi += 5; }

        else if (beratBarang > 50) {
            hargaPengiriman += 20000;
            estimasi += 3; }

        else if (beratBarang > 20) {
            hargaPengiriman += 10000;
            estimasi += 1; }

        int hari1 = Integer.parseInt(hari2.substring(0, 2));
        int bulan = Integer.parseInt(hari2.substring(3, 5));

        if (hari1 < 1 || hari1 > 31 || bulan < 1 || bulan > 12 || hari2.length() !=10) {
            System.out.println("Input tanggal tidak sesuai!");
            return; }

        int tahun = Integer.parseInt(hari2.substring(6));
        hari1 += estimasi;

        switch (bulan) {
            case 1: {
                if (hari1 > 31) {
                    hari1 = hari1 - 31;
                    bulan++;
                    break; } }
            case 2: {
                if (tahun % 4 == 0) {
                    if (hari1 > 29){
                        hari1 = hari1 - 29;
                        bulan++;
                        break; } }
                else {
                    if (hari1 > 28) {
                        hari1 = hari1 - 28;
                        bulan++;
                        break; } } }
            case 3: {
                if (hari1 > 31) {
                    hari1 = hari1 - 31;
                    bulan++;
                    break; } }
            case 4: {
                if (hari1 > 30) {
                    hari1 = hari1 - 30;
                    bulan++;
                    break; } }
            case 5: {
                if (hari1 > 31) {
                    hari1 = hari1 - 31;
                    bulan++;
                    break; } }
            case 6: {
                if (hari1 > 30) {
                    hari1 = hari1 - 30;
                    bulan++;
                    break; } }
            case 7: {
                if (hari1 > 31) {
                    hari1 = hari1 - 31;
                    bulan++;
                    break; } }
            case 8: {
                if (hari1 > 31) {
                    hari1 = hari1 - 31;
                    bulan++;
                    break; } }
            case 9: {
                if (hari1 > 30) {
                    hari1 = hari1 - 30;
                    bulan++;
                    break; } }
            case 10: {
                if (hari1 > 31) {
                    hari1 = hari1 - 31;
                    bulan++;
                    break; } }
            case 11: {
                if (hari1 > 30) {
                    hari1 = hari1 - 30;
                    bulan++;
                    break; } }
            case 12: {
                if (hari1 > 31) {
                    hari1 = hari1 - 31;
                    bulan++;
                    break; } }

            if (bulan > 12) {
                bulan -= 12;
                tahun++; }

            if (beratBarang < 0 || beratBarang > 2000) {
                return; }

            if (!tipeEkspedisi.equalsIgnoreCase("regular") && !tipeEkspedisi.equalsIgnoreCase("express") && !tipeEkspedisi.equalsIgnoreCase("super")) {
                return; }

            int tanggal = hari1;

            //kode kalian selese
            System.out.println("Nama barang: " + nama);
            System.out.println("Tanggal sampai barang: " + tanggal + "/" + bulan + "/" + tahun);
            System.out.print("Biaya pengiriman: ");
            System.out.printf("Rp %,.2f", hargaPengiriman);


        }
    }
}