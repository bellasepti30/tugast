package com.company;

import java.util.Objects;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String namaBarang = input.nextLine();
        double beratBarang = input.nextFloat();
        input.nextLine();
        String ekspedisi = input.nextLine();
        String tanggal = input.nextLine();

        Boolean verifyTanggal = true;
        Boolean verifyBerat = true;
        Boolean verifyEkspedisi = true;

        double hargaBarang = 0;
        int lamaPengiriman = 0;


        //BERAT
        if (beratBarang > 2000 || beratBarang <0) {
            verifyBerat=false;
        } else if (beratBarang>100) {
            lamaPengiriman += 5;
            hargaBarang += 30_000;
        } else if (beratBarang>50) {
            lamaPengiriman += 3;
            hargaBarang += 20_000;
        } else if (beratBarang>20) {
            lamaPengiriman += 1;
            hargaBarang += 10_000;
        }


        //EKSPEDISI
        switch (ekspedisi) {
            case "Regular":
                lamaPengiriman += 5;
                hargaBarang = 10_000;
                break;
            case "Express":
                lamaPengiriman += 3;
                hargaBarang = 20_000;
                break;
            case "Super":
                lamaPengiriman += 1;
                hargaBarang = 50_000;
                break;
            default:
                verifyEkspedisi=false;
        }


        //TANGGAL
        int hari = 0;
        int bulan = 0;
        int tahun = 0;

        if (tanggal.charAt(2)!='/' || tanggal.charAt(5)!='/') {
            verifyTanggal=false;
        } else {
            hari = Integer.parseInt(tanggal.substring(0,2));
            bulan = Integer.parseInt(tanggal.substring(3,5));
            tahun = Integer.parseInt(tanggal.substring(6,10));
        }

        //HARI DITERIMA
        int hariDiterima = hari + lamaPengiriman;

        //BULAN DITERIMA
        if (bulan==1 || bulan==3 || bulan==5 || bulan==7 || bulan==10 || bulan==12) {
            while (hari>31) {
                hari-=31;
                bulan++;
            }
        } else if ((bulan==4 || bulan==6 || bulan==8 || bulan==9 || bulan==11)) {
            while (hari>30) {
                hari-=30;
                bulan++;
            }
        } else if (bulan==2) {
            if (tahun%400!=0 && tahun%100==0) {
                while (hari>28) {
                    hari-=28;
                    bulan++;
                }
            } else {
                while (hari>29) {
                    hari-=29;
                    bulan++;
                }
            }
        }

        //TAHUN DITERIMA
        if (bulan>12){
            bulan-=12;
            tahun++;
        }


        //OUTPUT
        if (verifyBerat==false || verifyEkspedisi==false || verifyTanggal==false) {
            if (verifyBerat==false) {
                System.out.println("Berat tidak sesuai ketentuan!");
            }
            if (verifyEkspedisi==false) {
                System.out.println("Pilihan ekspedisi tidak valid!");
            }
            if (verifyTanggal==false) {
                System.out.println("Input tanggal tidak sesuai!");
            }
        } else {
            System.out.printf("Nama barang: %s\nTanggal sampai barang: %d/%d/%d\nBiaya pengiriman: Rp %,.2f", namaBarang, hariDiterima, bulan, tahun, hargaBarang);
        }}}
       /* String namaLengkapMHS, jenisGender;
        int umurMHS, tahunIni, tahunKelahiran;
        double beratBadanMHS, tinggiMHS;
        double idealFormula;

        //Input biodata mahasiswa
        namaLengkapMHS = inputkanNilai.nextLine();
        tahunKelahiran = inputkanNilai.nextInt();
        jenisGender = inputkanNilai.next();
        tinggiMHS = inputkanNilai.nextDouble();

        //Input tahun lahir untuk menghitung umur
        tahunIni = 2023;
        umurMHS = (int) tahunIni - tahunKelahiran;

        //Menentukan ukuran berat badan ideal sesuai gender
            if (jenisGender.equals("Pria")) {
                idealFormula = (double) (tinggiMHS - 100 - ((tinggiMHS - 100) / 10));
                System.out.print("Nama: " + namaLengkapMHS + "\n" +
                        "Umur: " + umurMHS + "\n" +
                        "Jenis Kelamin: " + jenisGender + "\n" +
                        "Berat Badan Ideal: " + idealFormula + " Kg");
            }
            else if (jenisGender.equals("Wanita")){
                idealFormula = (double) (tinggiMHS - 100 - ((tinggiMHS - 104) / 10));
                System.out.print("Nama: " + namaLengkapMHS + "\n" +
                        "Umur: " + umurMHS + "\n" +
                        "Jenis Kelamin: " + jenisGender + "\n" +
                        "Berat Badan Ideal: " + idealFormula + " Kg");
            }
            else {
                System.out.println("Input data Jenis Kelamin tidak sesuai.");
            }*/
       /*int i;
       int a;
        for (i = 1; i<=3; ++i){
            for (a = 0; a < 5; a++){
                System.out.print("*");
            }
            System.out.println();
        }
        //System.out.println(i);//i++ 3 jadi 4 namanya increment
        //}
        /*int x = 10;
        int y = 20;
        int z;
        z=x++;    // di print dulu trs ditambah
        System.out.println(z);
        z=++y;    // ditambah dulu trs di print
        System.out.println(z);*/

        //while ngerti dulu kondisinya true/false baru jalan
        /*boolean f = true;
        while (f){ //while akan bekerja saat kondisi true (terpenuhi)
            System.out.println("a");
            String JK = new Scanner (System.in).nextLine();
            if (!Objects.equals(JK,"P") && !Objects.equals(JK,"L")) {
                f = false;
                System.out.println("salah");
            } else {
                System.out.println(JK);
            }}*/

        // do ngerjain dulu
         /*  boolean a = true; // karena false maka di while ga akan ngulang, kalo true baru ngulang
           do {
               System.out.println('a');
           } while(a);
*/

        //karena a++ jadi udah dihitung dulu trs di print trs baru break
      /*  for (int a = 1; a <= 10; a++){
            if (a==7){
                continue;
            }
            System.out.println(a);
        }*/
      /*  Scanner inputkanNilai = new Scanner(System.in);
       // int nim = inputkanNilai.nextInt();
     //   int fak , pro , jal , no, ang;

       String nim = inputkanNilai.nextLine();
        String fak = null;
        String ang = nim.substring(0, 1);
        String pro = null;
        String jal= null;
        String no = null;

        if (nim.length()==15) {

            if (nim.substring() == 23) {

            }
            else if (fak == nim.substring(2,5)) {
                System.out.println("Fakultas = " + fak);
            }
            else if (pro == nim.substring(6, 7)) {
                System.out.println("Fakultas = " + pro);
            }
            else if (jal == nim.substring(8,11)) {
                System.out.println("Fakultas = " + jal);
            }
            else if (no == nim.substring(12,14)) {
                System.out.println("Fakultas = " + no);
            }
             /*String kata = input.nextLine();
            int jumlahKata = 1;

            if (kata.isEmpty()) {
                jumlahKata = 0;
            }

            for (int i = 0; i <kata.length(); i++) {
                if(kata.substring(i, (i + 1)).equals(" ")){
                    jumlahKata++;
                }
            }

            System.out.println(jumlahKata);

                    String nama, prodi, nim, barang, hari;
                    int jumlahBarang, angkatan;
                    double harga, totalHarga, totalDiskonHari = 0, totalDiskonJumlah = 0, totalAkhir;

                    System.out.println("Selamat Datang di Filkom  Mart");
                    System.out.println("Masukkan nama anda");
                    nama = input.nextLine();
                    System.out.println("Masukkan NIM anda");
                    nim = input.nextLine();
                    System.out.println("Masukkan Program Studi anda");
                    prodi = input.nextLine();
                    System.out.println("Masukkan Angkatan anda");
                    angkatan = input.nextInt();
                    System.out.println("Masukkan Hari");
                    input.nextLine();
                    hari = input.nextLine();
                    System.out.println("Masukan barang yang mau dibeli");
                    barang = input.nextLine();
                    System.out.println("Masukkan harga barang yang mau dibeli");
                    harga = input.nextDouble();
                    System.out.println("Masukan jumlah barang yang mau dibeli");
                    jumlahBarang = input.nextInt();

                    totalHarga = harga * jumlahBarang;

                    switch(prodi){
                        case "SI" -> {
                            if("Senin".equals(hari)){
                                totalDiskonHari = totalHarga * 0.05;
                            }
                        }
                        case "TI" -> {
                            if("Selasa".equals(hari)){
                                totalDiskonHari = totalHarga * 0.05;
                            }
                        }
                        case "PTI" -> {
                            if("Rabu".equals(hari)){
                                totalDiskonHari = totalHarga * 0.05;
                            }
                        }
                        case "TIF" -> {
                            if("Kamis".equals(hari)){
                                totalDiskonHari = totalHarga * 0.05;
                            }
                        }
                        case "TEKKOM" -> {
                            if("Jumat".equals(hari)){
                                totalDiskonHari = totalHarga * 0.05;
                            }
                        }
                    }

                    if(jumlahBarang > 21){
                        totalDiskonJumlah = totalHarga * 0.12;
                    }
                    else if(jumlahBarang > 16){
                        totalDiskonJumlah = totalHarga * 0.07;
                    }
                    else if(jumlahBarang > 9){
                        totalDiskonJumlah = totalHarga * 0.03;
                    }

                    totalAkhir = totalHarga - (totalDiskonHari + totalDiskonJumlah);

                    System.out.println("==================================================================");
                    System.out.println("Nama\t\t\t= "  + nama);
                    System.out.println("Nim\t\t\t\t= "  + nim);
                    System.out.println("Program Studi\t= "  + prodi);
                    System.out.println("Angkatan\t\t= "  + angkatan);

                    System.out.println("==================================================================");
                    System.out.printf("%-5s%-2s%-12s%-2s%-12s%-2s%-30s|\n", "No.", "|", "Barang", "|", "Jumlah", "|", "Harga satuan", "|");
                    System.out.printf("%-5s%-2s%-12s%-2s%-12d%-2sRp %,-27.2f|\n", "1", "|", barang, "|", jumlahBarang,  "|", harga, "|");

                    System.out.println("==================================================================");
                    System.out.printf("%-33s%-2sRp %,4.2f%19s\n",  "Total", "|", totalAkhir, "|");

            String golo = input.nextLine();
            String hasil = "Bisa mendonorkan darah ke ";
            switch (golo){

                case "A+"-> {
                    hasil +="A+ dan AB+";
                }
                case "A-"-> {
                   hasil += "A+, A-, AB+, dan AB-";
                }
                case "B+"->{
                    hasil +="B+ dan AB+";
                }
                case "B-"-> {
                    hasil+= "B+, B-, AB+, dan AB-";
                }
                case "AB+"-> {
                    hasil+="AB+";
                }
                case "AB-"->{
                   hasil +="AB+ dan AB-";
                }
                case "O+"->{
                   hasil+="O+, A+, B+, dan AB+";
                }
                case "O-"->{
                   hasil+="delapan golongan darah";
                }
                default -> {
                    hasil = "inputan salah";
                }
            }
             String golo = input.nextLine();
            String hasil = "Bisa mendonorkan darah ";
              if (golo.equalsIgnoreCase("A+")) {
            hasil += "ke golongan darah A+ dan AB+";
        } else if (golo.equalsIgnoreCase("A-")) {
            hasil += "ke golongan darah A+, A-, AB+, dan AB-";
        } else if (golo.equalsIgnoreCase("B+")) {
            hasil += "ke golongan darah B+ dan AB+";
        } else if (golo.equalsIgnoreCase("B-")) {
            hasil += "ke golongan darah B+, B-, AB+, dan AB-";
        } else if (golo.equalsIgnoreCase("AB+")) {
            hasil += "untuk golongan darah AB+";
        } else if (golo.equalsIgnoreCase("AB-")) {
            hasil += "untuk golongan darah AB+ dan AB-";
        } else if (golo.equalsIgnoreCase("O+")) {
            hasil += "ke golongan darah O+, A+, B+, dan AB+";
        } else if (golo.equalsIgnoreCase("O-")) {
            hasil += "ke delapan golongan darah";
        } else {
            hasil = "Inputan salah";
        }

        System.out.println(hasil);
        }

            int nilai = -1;

            if (nilai < 0) {
                System.out.println("Nilai negatif tidak valid.");
                System.exit(5); // Menghentikan program dengan status keluar 1 (biasanya digunakan untuk menunjukkan kesalahan)
            } else {
                System.out.println("Nilai valid: " + nilai);
            }

            // Kode di bawah ini tidak akan dieksekusi setelah System.exit() dipanggil.
            System.out.println("Ini tidak akan dicetak.");*/
          /*  int teoriN = input.nextInt();
            int praktikN = input.nextInt();
            int kerjasamaN = input.nextInt();
           String hurufMutu_N = " ";
            double angkaMutu_N = 0.0;
            String predikat_N = " ";
            double nilaiAkhir;
            nilaiAkhir = (double) ((teoriN*0.4) + (praktikN*0.4) + (kerjasamaN*0.2));



                if (nilaiAkhir > 80 && nilaiAkhir <= 100) {
                    hurufMutu_N = "A";
                    angkaMutu_N = 4.0;
                    predikat_N = "Sangat Baik";
                }
                else if (nilaiAkhir > 75 && nilaiAkhir <= 80)  {
                    hurufMutu_N = "B+";
                    angkaMutu_N = 3.5;
                    predikat_N = "Lebih Baik";
                }
                else if (nilaiAkhir > 69 && nilaiAkhir <= 75)  {
                    hurufMutu_N = "B";
                    angkaMutu_N = 3.0;
                    predikat_N = "Baik";
                }
                else if (nilaiAkhir > 60 && nilaiAkhir <= 69)  {
                    hurufMutu_N = "C+";
                    angkaMutu_N = 2.5;
                    predikat_N = "Lebih dari Cukup";
                }
                else if (nilaiAkhir > 55 && nilaiAkhir <= 60)  {
                    hurufMutu_N = "C";
                    angkaMutu_N = 2.0;
                    predikat_N= "Cukup";
                }
                else if (nilaiAkhir > 50 && nilaiAkhir <= 55)  {
                    hurufMutu_N = "D+";
                    angkaMutu_N = 1.5;
                    predikat_N = "Lebih dari Kurang";
                }
                else if (nilaiAkhir > 44 && nilaiAkhir <= 50)  {
                    hurufMutu_N = "D";
                    angkaMutu_N= 1.0;
                    predikat_N = "Kurang";
                }
                else  {
                    hurufMutu_N= "E";
                    angkaMutu_N = 0.0;
                    predikat_N = "Gagal";

            }

            System.out.printf("%s | %.1f | %s\n", hurufMutu_N, angkaMutu_N, predikat_N);*/

 /*System.out.print("Masukkan tanggal awal (dd/mm/yyyy): ");
            String startDateStr = input.nextLine();
            System.out.print("Masukkan jumlah hari yang akan ditambahkan: ");
            int daysToAdd = input.nextInt();

            String[] dateParts = startDateStr.split("/");
            if (dateParts.length != 3) {
                System.out.println("Format tanggal tidak sesuai!");
                return;
            }

            int day = Integer.parseInt(dateParts[0]);
            int month = Integer.parseInt(dateParts[1]);
            int year = Integer.parseInt(dateParts[2]);

            for (int i = 0; i < daysToAdd; i++) {
                day++;

                if (day > 31) {
                    day = 1;
                    month++;

                    if (month > 12) {
                        month = 1;
                        year++;
                    }
                }
                 String[] tanggal_kirimSplit = tanggal_kirim.split("/");
             hari = Integer.parseInt(tanggal_kirimSplit[0]);
             bulan = Integer.parseInt(tanggal_kirimSplit[1]);
             tahun = Integer.parseInt(tanggal_kirimSplit[2]);
            }*/