package com.company;
import java.util.Scanner;

public class PraktikumCodee{

    public static void main (String [] args){
        Scanner inputanUser = new Scanner(System.in);
        int nilaiOpsi;
        do{
            System.out.println("\n");
            System.out.println("MENU :");
            System.out.println("0. KELUAR");
            System.out.println("1. HITUNG VOLUME BALOK");
            System.out.println("2. HITUNG VOLUME BOLA");
            System.out.println("3. HITUNG VOLUME KERUCUT");
            System.out.println("4. HITUNG VOLUME SILINDER");
            System.out.println("5. HITUNG VOLUME LIMAS SEGITIGA");
            System.out.print("\tMASUKKAN PILIHAN ANDA : ");

            nilaiOpsi = inputanUser.nextInt();
            System.out.println();

            if (nilaiOpsi == 0) {
                System.out.println("Telah Keluar dari Program. Terima Kasih..");
                break;
            }
            else if (nilaiOpsi == 1 ) {
                double p_b,l_b,t_b,rumus_v_balok; // p_B=panjang  l_B=lebar  t_B=tinggi
                System.out.println("Hitung Volume dari Balok");
                System.out.print("Masukkan Nilai Panjang\t: ");
                p_b = inputanUser.nextDouble();
                System.out.print("Masukkan Nilai Lebar\t: ");
                l_b = inputanUser.nextDouble();
                System.out.print("Masukkan Nilai Tinggi\t: ");
                t_b = inputanUser.nextDouble();
                rumus_v_balok = p_b * l_b * t_b;
                System.out.printf("Hasil Volume Balok\t\t: " + "%.2f" , rumus_v_balok);
                continue;
            }
            else if (nilaiOpsi == 2){
                float phi,r_bo, rumus_V_bola; //  r_Bo=radius
                phi = 3.14f;
                System.out.println("Hitung Volume dari Bola");
                System.out.print("Masukkan Nilai Radius\t: ");
                r_bo = inputanUser.nextFloat();
                rumus_V_bola =(float) (4.0 / 3.0 * phi * Math.pow(r_bo , 3));
                System.out.printf("Hasil Volume Bola\t\t: " + "%.2f" , rumus_V_bola);
                continue;
            }
            else if (nilaiOpsi == 3){
                float phi,r_k, t_k, rumus_v_kerucut; //  r_K=radius t_K=tinggi
                phi = 3.14f;
                System.out.println("Hitung Volume dari Kerucut");
                System.out.print("Masukkan Nilai Radius\t: ");
                r_k = inputanUser.nextFloat();
                System.out.print("Masukkan Nilai Tinggi\t: ");
                t_k = inputanUser.nextFloat();
                rumus_v_kerucut = (float) (phi * Math.pow(r_k , 2) * t_k) / 3;
                System.out.printf("Hasil Volume Kerucut\t: " + "%.2f", rumus_v_kerucut);
                continue;
            }
            else if (nilaiOpsi == 4){
                float phi, r_s, t_s, rumus_v_silinder; //  r_s=radius t_s=tinggi
                phi = 3.14f;
                System.out.println("Hitung Volume dari Silinder");
                System.out.print("Masukkan Nilai Radius\t: ");
                r_s = inputanUser.nextFloat();
                System.out.print("Masukkan Nilai Tinggi\t: ");
                t_s = inputanUser.nextFloat();
                rumus_v_silinder = (float) (phi * Math.pow(r_s , 2) * t_s);
                System.out.printf("Hasil Volume Silinder\t: " + "%.2f" , rumus_v_silinder);
                continue;
            }
            else if (nilaiOpsi == 5){
                double p_li, l_li, t_li,rumus_v_limas; //  p_li=panjang  l_li=lebar  t_li=tinggi
                System.out.println("Hitung Volume dari Limas Segitiga");
                System.out.print("Masukkan Nilai Panjang\t\t: ");
                p_li = inputanUser.nextDouble();
                System.out.print("Masukkan Nilai Lebar\t\t: ");
                l_li = inputanUser.nextDouble();
                System.out.print("Masukkan Nilai Tinggi\t\t: ");
                t_li = inputanUser.nextDouble();
                rumus_v_limas =  ((p_li * l_li / 2.0) * t_li) / 3.0;
                System.out.printf("Hasil Volume Limas Segitiga\t: " + "%.2f" , rumus_v_limas);
                continue;
            }
            else {
                System.out.print("Gagal. Masukkan Angka 0 hingga 5 Saja.");
                continue;
            }
        } while(nilaiOpsi!=0);

    inputanUser.close();
    }
}