/*package com.company;
import java.util.Scanner;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ProgramDasar {
// (NIM = 235150707111002)
// 6.12
// (Display Character) Write a method that prints characters from 1 to Z with ten char per line
   /* public static void main (String []args) {
        // main method that declare ch1, ch2, numberPerLine
        char ch1 = '1'; // as the start character
        char ch2 = 'Z'; // as the end character
        final int numberPerLine = 10; // final as a fixed number that the limit cannot be changed

        System.out.println("Ten Character per Line from 1 to Z"); // to display what the purpose this programs is
        System.out.println();
        printChars (ch1, ch2, numberPerLine); // to declare the method with parameters to be called
    }

    public static void printChars (char ch1, char ch2, int numberPerLine){
        //method printChars is called to print the characters
        for (char k = ch1, count = 1; k <= ch2; k++, count++) {
                System.out.print(k + " ");

            if (count % numberPerLine == 0){ // if count had reached (10) and 10/10 becomes no reminder
                System.out.println(); // so the character after the 10th will be display on new line
            }
        }
        System.out.println();
    }*/

    /*public static void main(String[] args) {
        recursiveMain(0,1, 5);
    }

     static void recursiveMain(int a, int b, int n) {
        if (n > 0) {
            System.out.print(a + " ");
            recursiveMain(b, a + b, n - 1);
            // Memanggil main secara rekursif dengan nilai n-1
        }
    }*/

        /*public static String riffleShuffle(String cards, int k) {
            int n = cards.length();
            int mid = n / 2;
            char[] cardArray = cards.toCharArray();

            for (int x = 0; x < k; x++) {
                char[] shuffledCards = new char[n];
                int index = 0;

                for (int i = 0; i < mid; i++) {
                    shuffledCards[index++] = cardArray[i];
                    shuffledCards[index++] = cardArray[i + mid];
                }

                cardArray = shuffledCards;
            }

            return new String(cardArray);
        }

        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);

            // Input data tumpukan kartu dan jumlah kali acak
            String tumpukanKartu = scanner.nextLine().trim();
            int jumlahAcak = Integer.parseInt(scanner.nextLine());

            // Memanggil fungsi riffleShuffle dan menampilkan hasil
            String hasilAcak = riffleShuffle(tumpukanKartu, jumlahAcak);
            System.out.println(hasilAcak);
        }*/

        /*public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);

            // Input panjang array
            int panjangArray = Integer.parseInt(scanner.nextLine());

            // Input isi array
            int[] array = Arrays.stream(scanner.nextLine().split(" "))
                    .mapToInt(Integer::parseInt)
                    .toArray();

            // Mengurutkan array
            Arrays.sort(array);

            // Menghitung total nilai terbesar
            int totalNilaiTerbesar = Arrays.stream(array).sum();

            // Menghitung median
            double median;
            if (panjangArray % 2 == 0) {
                median = (array[panjangArray / 2 - 1] + array[panjangArray / 2]) / 2.0;
            } else {
                median = array[panjangArray / 2];
            }

            // Menghitung modus
            Map<Integer, Integer> counter = new HashMap<>();
            for (int num : array) {
                counter.put(num, counter.getOrDefault(num, 0) + 1);
            }

            int maxCount = 0;
            int modus = 0;
            for (Map.Entry<Integer, Integer> entry : counter.entrySet()) {
                if (entry.getValue() > maxCount || (entry.getValue() == maxCount && entry.getKey() > modus)) {
                    maxCount = entry.getValue();
                    modus = entry.getKey();
                }
            }

            // Menampilkan hasil
            System.out.println("Total Nilai Terbesar = " + totalNilaiTerbesar);
            System.out.println("Median = " + median);
            System.out.println("Modus = " + modus);
        }*/


       /* public static String shuffleEven(String cards) {
            //panjang kartu genap
            int even = cards.length();
            int middle = even / 2;

            StringBuilder result = new StringBuilder();
            //for (int x = 0; x < k; x++) {
              //  char[] shuffledCards = new char[even];
            //    int index = 0;

                for (int i = 0; i < middle; i++) {
                    if (even % 2 == 0){
                    //result[index++].(cards.charAt(i);
                    //result[index++] = cardArray[i + middle];
                        result.append(cards.charAt(i));
                        result.append(cards.charAt(i+middle));
                    }
            }
            return result.toString();
        }

            public static String shuffleOdd(String cards) {
                //panjang kartu ganjil
                int odd = cards.length();
                int middle = odd / 2;
                //char[] cardArray = cards.toCharArray();

                String satu = cards.substring(0, middle + 1);
                String dua = cards.substring(middle + 1);

                StringBuilder result = new StringBuilder();
               // for (int x = 0; x < k; x++) {
                  //  char[] shuffledCards = new char[even];
                  //  int index = 0;

                    for (int i = 0; i < middle; i++) {
                        result.append(satu.charAt(i));
                        if (i != middle)
                            result.append(dua.charAt(i));
                        if (i == middle - 1)
                            result.append(satu.charAt(i + 1));
                    }
                    return result.toString();
                    }

            public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);

            // Input data tumpukan kartu dan jumlah kali acak
            String tumpukanKartu = scanner.nextLine().strip();
            int jumlahAcak = scanner.nextInt();

                    if (tumpukanKartu.length() % 2 == 0) {
                        for (int i = 0; i < jumlahAcak; i++) {
                            tumpukanKartu = shuffleEven(tumpukanKartu);
                        }
                    } else if (tumpukanKartu.length() % 2 != 0) {
                        for (int i = 0; i < jumlahAcak; i++) {
                            tumpukanKartu = shuffleOdd(tumpukanKartu);
                        }
                    }
                    System.out.println(tumpukanKartu);

                }
            // Memanggil fungsi riffleShuffle dan menampilkan hasil
            /*String hasilAcak = shuffle(tumpukanKartu, jumlahAcak);
            System.out.println(hasilAcak);
        }
    }*/



   /* public void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input panjang array
        int panjangArray = scanner.nextInt();

        // Input isi array
        double[] array = new double[panjangArray];
        for (int i = 0; i < panjangArray; i++) {
            array[i] = scanner.nextDouble();
        }
        // Mengurutkan array
        Arrays.sort(array);


        // Menghitung total nilai terbesar
        int totalNilaiTerbesar = (int) jumlahTerbesar(array);
        System.out.println("Total Nilai Terbesar = " + totalNilaiTerbesar);

        double median = lookMedian(array);
        System.out.println("Median = " + median);

        double modus = lookModus(array);
        System.out.println("Modus = " + modus);
    }

    public static double jumlahTerbesar(double[] array) {
        double jumlah = 0;
        double max = array[array.length - 1];
        for (int i = 0; i < array.length; i++) {
            if (array[i] == max) {
                jumlah += array[i];
            }
        }
        return jumlah;
    }

    // Menghitung median
    private static  double lookMedian(double[] array) {
        int panjang = array.length;
        if (panjang % 2 == 0) {
            double median1 = array[panjang / 2 - 1];
            double median2 = array[panjang / 2];
            return (median1 + median2) / 2.0;
        } else {
            return array[panjang/ 2];
        }
    }

    private static  double lookModus(double[] array) {
        double modus = array[0];
        int hitung = 1;
        int maxHitung = 1;

        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1]) {
                hitung++;
            } else {
                hitung = 1;
            }
            if (hitung >= maxHitung) {
                maxHitung = hitung;
                modus = array[i];
            }
        }
        return modus;
    }*/
package com.company;
import java.util.Scanner;

public class ProgramDasar {

    static class TakoyakiItem {
        String name;
        int price;

        TakoyakiItem(String name, int price) {
            this.name = name;
            this.price = price;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        TakoyakiItem[] takoyakiItems = {
                new TakoyakiItem("Takoyaki Pentol", 5000),
                new TakoyakiItem("Takoyaki Keju", 6000),
                new TakoyakiItem("Takoyaki Sosis", 7000),
                new TakoyakiItem("Takoyaki Kepiting", 8000),
                new TakoyakiItem("Takoyaki Lobster", 9000)
        };

        int[] totalTakoyakiItems = new int[takoyakiItems.length];

        do {
            System.out.println("-------------TAKOYAKI POINT OF SALE------------");
            System.out.println("1. Buy Takoyaki");
            System.out.println("2. Update Takoyaki Price");
            System.out.println("3. Owner Menu");
            System.out.println("4. Checkout");
            System.out.println("-------------------------------------------------");
            System.out.print("Enter your choice: ");
            int pilih1 = sc.nextInt();

            switch (pilih1) {
                case 1:
                    buyTakoyaki(sc, takoyakiItems, totalTakoyakiItems);
                    break;
                case 2:
                    updateTakoyakiPrice(sc, takoyakiItems);
                    break;
                case 3:
                    ownerMenu(totalTakoyakiItems);
                    break;
                case 4:
                    checkout(totalTakoyakiItems);
                    break;
            }
        } while (true);
    }

    private static void buyTakoyaki(Scanner sc, TakoyakiItem[] takoyakiItems, int[] totalTakoyakiItems) {
        System.out.println("Choose Takoyaki to Buy: ");
        for (int i = 0; i < takoyakiItems.length; i++) {
            System.out.println((i + 1) + ". " + takoyakiItems[i].name + " - Rp. " + takoyakiItems[i].price);
        }
        System.out.print("Enter your choice: ");
        int choice = sc.nextInt();
        System.out.print("Enter the quantity: ");
        int quantity = sc.nextInt();

        totalTakoyakiItems[choice - 1] += quantity;

        System.out.println("You have bought " + quantity + " " + takoyakiItems[choice - 1].name + "(s)");
        System.out.println("");
    }

    private static void updateTakoyakiPrice(Scanner sc, TakoyakiItem[] takoyakiItems) {
        System.out.println("Choose Takoyaki to Update Price: ");
        for (int i = 0; i < takoyakiItems.length; i++) {
            System.out.println((i + 1) + ". " + takoyakiItems[i].name + " - Rp. " + takoyakiItems[i].price);
        }
        System.out.print("Enter your choice: ");
        int choice = sc.nextInt();
        System.out.print("Enter the new price: ");
        int newPrice = sc.nextInt();

        takoyakiItems[choice - 1].price = newPrice;

        System.out.println("Price for " + takoyakiItems[choice - 1].name + " has been updated to Rp. " + newPrice);
        System.out.println("");
    }

    private static void ownerMenu(int[] totalTakoyakiItems) {
        System.out.println("Owner Menu: ");
        for (int i = 0; i < totalTakoyakiItems.length; i++) {
            System.out.println((i + 1) + ". " + totalTakoyakiItems[i]);
        }
        System.out.println("");
    }

    private static void checkout(int[] totalTakoyakiItems) {
        int totalAmount = 0;
        for (int i = 0; i < totalTakoyakiItems.length; i++) {
            totalAmount += totalTakoyakiItems[i];
        }
        System.out.println("You have bought a total of " + totalAmount + " Takoyaki items.");
        System.out.println("Thank you for visiting. Please come again!");
        System.exit(0);
    }
}

