package com.company;

import java.util.Scanner;

   /* public class Kelass{
        public static void main (String[]args) {
            Scanner input = new Scanner(System.in);
            int opsi;
            do {
                System.out.println("""
                        \nMenu Konversi Bilangan:
                        1. Biner ke Desimal
                        2. Desimal ke Biner
                        3. Biner ke Hexadesimal
                        4. Hexadesimal ke Biner
                        5. Desimal ke Hexadesimal
                        6. Hexadesimal ke Desimal
                        7. EXIT
                        """);
                System.out.print("Pilih Operasi dari Opsi 1 hingga 6 => ");
                opsi = input.nextInt();
                System.out.println();

                switch (opsi) {
                    case 1-> biner_ke_desimal();
                    case 2-> desimal_ke_biner();
                    case 3-> biner_ke_hexadesimal();
                    case 4-> hexadesimal_ke_biner();
                    case 5-> desimal_ke_hexadesimal();
                    case 6-> hexadesimal_ke_desimal();
                    case 7-> System.out.println("Anda Telah Keluar dari Program, Terima Kasih..");
                    default-> System.out.println("Maaf, Opsi Tidak Sesuai pada Pilihan yang Ada");
                }

            } while (opsi!=7);

        }

            public static void biner_ke_desimal () {
                System.out.println("Konversi Bilangan Biner ke Desimal");
                Scanner input = new Scanner(System.in);
                System.out.print("Masukkan Bilangan Biner: ");
                String nilaiBiner = input.nextLine();
                int hasilDesimal = Integer.parseInt(nilaiBiner, 2);
                System.out.println("Hasil Konversi dalam Bilangan Desimal: " + hasilDesimal);
            }

            public static void desimal_ke_biner () {
                System.out.println("Konversi Bilangan Desimal ke Biner");
                Scanner input = new Scanner(System.in);
                System.out.print("Masukkan Bilangan Desimal: ");
                int nilaiDesimal = input.nextInt();
                String hasilBiner = Integer.toBinaryString(nilaiDesimal);
                System.out.println("Hasil Konversi dalam Bilangan Biner: " + hasilBiner);
            }

            public static void biner_ke_hexadesimal () {
                System.out.println("Konversi Bilangan Biner ke Hexadesimal");
                Scanner input = new Scanner(System.in);
                System.out.print("Masukkan Bilangan Biner: ");
                String nilaiBiner = input.nextLine();
                int desimal = Integer.parseInt(nilaiBiner, 2);
                String hasilHexadesimal = Integer.toHexString(desimal).toUpperCase();
                System.out.println("Hasil Konversi dalam Bilangan Hexadesimal: " + hasilHexadesimal);
            }

            public static void hexadesimal_ke_biner () {
                System.out.println("Konversi Bilangan Hexadesimal ke Biner");
                Scanner input = new Scanner(System.in);
                System.out.print("Masukkan Bilangan Hexadesimal: ");
                String nilaiHexadesimal = input.nextLine();
                int desimal = Integer.parseInt(nilaiHexadesimal, 16);
                String hasilBiner = Integer.toBinaryString(desimal);
                System.out.println("Hasil Konversi dalam Bilangan Biner: " + hasilBiner);
            }

            public static void desimal_ke_hexadesimal () {
                System.out.println("Konversi Bilangan Desimal ke Hexadesimal");
                Scanner input = new Scanner(System.in);
                System.out.print("Masukkan Bilangan Desimal: ");
                int nilaiDesimal = input.nextInt();
                String hasilHexadesimal = Integer.toHexString(nilaiDesimal).toUpperCase();
                System.out.println("Hasil Konversi dalam Bilangan Hexadesimal: " + hasilHexadesimal);
            }

            public static void hexadesimal_ke_desimal () {
                System.out.println("Konversi Bilangan Hexadesimal ke Desimal");
                Scanner input = new Scanner(System.in);
                System.out.print("Masukkan Bilangan Hexadesimal: ");
                String nilaiHexadesimal = input.nextLine();
                int hasilDesimal = Integer.parseInt(nilaiHexadesimal, 16);
                System.out.println("Hasil Konversi dalam Bilangan Desimal: " + hasilDesimal);
            }
            //pada (//kondisi) => int mulai_j = (i2 == i1) ? j1 + 1 : 0;
            // => untuk kode bawah antara for (int p = x; p < height; p++) { dan if (x == p)
*/
  /* public class Kelass {
       public static void main(String[] args) {
           Scanner in = new Scanner(System.in);

           // Membaca input lebar dan tinggi array 2D dan target hasil
           int height = in.nextInt();
           int width = in.nextInt();
           int target = in.nextInt();
           // Menyimpan ukuran array 2D dengan tinggi * lebar
           int[][] array = new int[height][width];

           for (int row = 0; row < (array).length; row++) { // iterasi baris dari array
               for (int column = 0; column < (array)[row].length; column++) { // iterasi kolom dari array
                   //Membaca tiap elemen dari input
                   array[row][column] = in.nextInt();
               }
           }
           // Mencari pasangan angka yang memenuhi target
           for (int i = 0; i < (array).length; i++) { // baris 1
               for (int j = 0; j < (array)[i].length; j++) { // kolom 1
                   for (int x = i; x < (array).length; x++) { // baris 2
                       for (int y = 0; y < (array)[x].length; y++) { // kolom 2

                           if (i >= x && y <= j) {
                               continue;
                           }
                           int a = array[i][j]; // baris 1 , kolom 1
                           int b = array[x][y]; // baris 2 , kolom 2

                           if (a + b == target) {
                               System.out.println("[" + i + "," + j + "] + [" + x + "," + y + "]");
                           }if (a - b == target) {
                               System.out.println("[" + i + "," + j + "] - [" + x + "," + y + "]");
                           }if (b - a == target) {
                               System.out.println("[" + x + "," + y + "] - [" + i + "," + j + "]");
                           }if (a * b == target) {
                               System.out.println("[" + i + "," + j + "] * [" + x + "," + y + "]");
                           }if (b != 0 && a % b == 0  && a / b == target) {
                               System.out.println("[" + i + "," + j + "] / [" + x + "," + y + "]");
                           }if (a != 0 && b % a == 0 && b / a == target) {
                               System.out.println("[" + x + "," + y + "] / [" + i + "," + j + "]");
                           }
                       }
                   }
               }
           }
       }
   }*/

/*public class Kelass {

    public static int[][] exampleVariableOne = new int[10][5];
    // returns the length of the rows in the array
    public static int lengthOne = exampleVariableOne.length;
    // returns the length of the columns in the array
    public static int lengthTwo = exampleVariableOne[0].length;

    public static void main(String[] args) {
        System.out.println(lengthOne);
        System.out.println(lengthTwo);
    }
}*/
/*public class Kelass {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan panjang fibonacci: ");
        int length = input.nextInt();

        System.out.print("Masukkan bilangan pertama : ");
        int first = input.nextInt();
        System.out.print("Masukkan bilangan kedua : ");
        int second = input.nextInt();


        System.out.println("Deret Fibonacci :");
        Fibonacci(first, second, length, 0);
    }

    static void Fibonacci(int first, int second, int length, int current) {

        if (current < length) {
            System.out.print(first + " ");
            Fibonacci(second, first + second, length, current + 1);
        }

    }
}*/
import java.util.Scanner;

public class Kelass {
    public static void main(String[] args) {
        Scanner c = new Scanner(System.in);

        System.out.print("Masukkan bilangan pertama: ");
        int bilYangKe1 = c.nextInt();

        System.out.print("Masukkan bilangan kedua: ");
        int bilYangKe2 = c.nextInt();

        System.out.print("Masukkan jumlah barisan Fibonacci yang ingin ditampilkan: ");
        int jumlahBarisan = c.nextInt();

        if (bilYangKe1 < 0 || bilYangKe2 < 0 || jumlahBarisan <= 0) {
            System.out.println("Masukkan bilangan positif dan jumlah barisan yang valid.");
        } else {
            // Menampilkan barisan Fibonacci dan menghitung jumlahnya
            System.out.println("Barisan Fibonacci:");

            if (jumlahBarisan >= 1) {
                System.out.print(bilYangKe1);
                if (jumlahBarisan > 1) {
                    System.out.print(", " + bilYangKe2);
                    System.out.print(bilYangKe1 + bilYangKe2 + jumlahBarisan-2);
                    // Memanggil fungsi rekursif untuk menghasilkan barisan Fibonacci
                    //generateFibonacci(bilYangKe1, bilYangKe2, jumlahBarisan - 2);
                }
                int a = 0,b = 0,count = 0;
                if (count > 0) {
                    int nextTerm = a + b;
                    System.out.print(", " + nextTerm);
                    System.out.println(b + nextTerm + count-1);
                    //generateFibonacci(b, nextTerm, count - 1);
                }
            }
        }

        // Menutup scanner
        c.close();
    }

    // Fungsi rekursif untuk menghasilkan barisan Fibonacci
    private static void generateFibonacci(int a, int b, int count) {
        if (count > 0) {
            int nextTerm = a + b;
            System.out.print(", " + nextTerm);
            generateFibonacci(b, nextTerm, count - 1);
        }
    }
}
