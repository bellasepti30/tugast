package com.company;
import java.util.Scanner;

public class ProgramDasar1 {
/*// (NIM = 235150707111002)
// 6.22
//(Math: approximate the square root) Implement the Babylonian by the following method that returns the square root of n.
//Then try to compare with method from java
        public static void main(String[] args) {
            // main method that declare n
            long n = 81; // for example input the root
            double s = sqRt(n); // to initialize method sqRt(n)

            //try to compare with method from java
            System.out.println( "The difference between Babylonian Square Root and Method from Java");
            System.out.println( "\nthe Babylonian square root method => \n" + n + " = " + s );
            System.out.println( "\nthe Math.sqrt(i) method => \n" + n + " = " + Math.sqrt(n) );
        }

        public static double sqRt (long n) {
            // method sqRt that called to compute square root with Babylonian's formula
            double lastGs = 1; //for example input guess
            double nextGs = (lastGs + n / lastGs) / 2;

            while (!bedaTebakan (nextGs, lastGs)) {
           //the negation of method bedaTebakan (if it is false) will keep run
                lastGs = nextGs; //In each iteration, last guess is updated to next guess,
                nextGs = (lastGs + n / lastGs) / 2; //the value of next guess is calculated based on formula

            }
            return nextGs;
            //it will be stored in variable of the sqRt method, after the loop is complete.
        }

        public static boolean bedaTebakan ( double k, double l ) {
        // method bedaTebakan that called to check the condition is true
            return Math.abs (k - l) < 0.0001;
            // If the condition of difference between next Guess and last Guess is less than 0.0001.
        }
    }*/


        public static void main(String[] args) {
            Scanner input = new Scanner(System.in);

            //Input jumlah mahasiswa dan banyak nilai
            int jumlahMahasiswa = input.nextInt();
            int banyakNilai = input.nextInt();

            //Deklarasi variabel untuk menyimpan
            String mahasiswaTertinggi = "";
            double rataRataTertinggi = -1;
            double totalNilai = 0;

            //Input data mahasiswa dan nilai
            for (int i = 1; i <= jumlahMahasiswa; i++) {

                String namaMahasiswa = input.next();

                double totalNilaiIndividu = 0;

                // Input nilai mahasiswa
                for (int j = 1; j <= banyakNilai; j++) {
                    double nilai = input.nextDouble();
                    totalNilaiIndividu += nilai;
                }

                // Menghitung rata-rata nilai mahasiswa per individu
                double rataRataMahasiswaIndividu = totalNilaiIndividu / banyakNilai;

                // Menampilkan rata-rata nilai mahasiswa per individu
                System.out.println("Rata-rata nilai mahasiswa " + namaMahasiswa + " adalah " + rataRataMahasiswaIndividu);

                // Memeriksa apakah rata-rata tertinggi
                if (rataRataTertinggi == -1 || rataRataMahasiswaIndividu > rataRataTertinggi) {
                    rataRataTertinggi = rataRataMahasiswaIndividu;
                    mahasiswaTertinggi = namaMahasiswa;
                }

                // Menambahkan rata-rata nilai mahasiswa per individu ke total nilai mahasiswa
                totalNilai += rataRataMahasiswaIndividu;
            }

            // Menghitung rata-rata kelas
            double rataRataKelas = totalNilai / jumlahMahasiswa;

            // Menampilkan output sesuai format
            System.out.println("Rata-rata kelas: " + rataRataKelas);
            System.out.println("Mahasiswa yang memiliki nilai tertinggi adalah " + mahasiswaTertinggi + " dengan nilai " + rataRataTertinggi );
        }
    }

