package com.company;

import java.awt.*;
import java.util.Scanner;

public class Try {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String nama = input.nextLine();
        double berat_barang = input.nextDouble();
        String tipe_ekspedisi = input.next().toLowerCase();
        input.nextLine();
        String tanggal_kirim = input.nextLine();

        int estimasi_kirim = 0;
        int tanggal;
        double hargaPengiriman = 0.0;
        int hari = 0, bulan = 0, tahun = 0;

        boolean tanggal_bener = true;
        boolean berat_bener = true;
        boolean ekspedisi_bener = true;

        // CABANG TIPE EKSPEDISI dan HARGA BARANG
        if (tipe_ekspedisi.equalsIgnoreCase("Regular")) {
            hargaPengiriman = 10_000.00;
            estimasi_kirim += 5;
        }
        else if (tipe_ekspedisi.equalsIgnoreCase("Express")) {
            hargaPengiriman = 20_000.00;
            estimasi_kirim += 3;
        }
        else if (tipe_ekspedisi.equalsIgnoreCase("Super")) {
            hargaPengiriman = 50_000.00;
            estimasi_kirim += 1;
        }
        else {
            ekspedisi_bener = false;
        }

        // CABANG BERAT BARANG dan TAMBAH HARGA BARANG
        if (berat_barang > 2000 || berat_barang < 0){
            berat_bener = false;
        }
        else if (berat_barang > 100) {
            estimasi_kirim += 5;
            hargaPengiriman += 30_000;
        }
        else if (berat_barang > 50) {
            estimasi_kirim +=3;
            hargaPengiriman += 20_000;
        }
        else if (berat_barang > 20) {
            estimasi_kirim += 1;
            hargaPengiriman += 10_000;
        }

        // CEK KONDISI FORMAT MEMASUKKAN URUT TANGGAL, BULAN, TAHUN
        if (tanggal_kirim.length() != 10 || tanggal_kirim.charAt(2) != '/' || tanggal_kirim.charAt(5) != '/') {
            tanggal_bener = false;
        }
        else {
            hari = Integer.parseInt(tanggal_kirim.substring(0, 2));
            bulan = Integer.parseInt(tanggal_kirim.substring(3, 5));
            tahun = Integer.parseInt(tanggal_kirim.substring(6, 10));
        }

        // PERHITUNGAN UNTUK HARI BARANG AKAN SAMPAI
        tanggal = hari + estimasi_kirim;

        // CABANG BULAN DITERIMA

        if (bulan == 2) {
            // TAHUN KABISAT ABAD BARU HABIS DIBAGI 400, BUKAN ABAD BARU HAIS DIBAGI 4
            // CEK KONDISI JIKA TIDAK SESUAI KONDISI DI ATAS
            if ((tahun % 4 == 0 && tahun % 100 != 0) || (tahun % 400 == 0)) {
                if (tanggal > 29) {
                    tanggal = tanggal - 29;
                    bulan++;
                }
            } else {
                if (tanggal > 28) {
                    tanggal = tanggal - 28;
                    bulan++;
                }
            }
        }
        else if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 ||
                bulan == 12) {

            //UNTUK TANGGAL 1-31
            if (tanggal > 31) {
                tanggal = tanggal - 31;
                bulan++;

                //UNTUK DESEMBER SELANJUTNYA BERGANTI KE JANUARI DAN BERTAMBAH TAHUN
                if (bulan > 12) {
                    bulan = 1;
                    tahun++;
                }
            }
        }
        else if (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11) {

            //UNTUK TANGGAL 1-30
            if (tanggal > 30) {
                tanggal = tanggal - 30;
                bulan++;
            }
        }

        // PESAN TAMPILAN UNTUK KESALAHAN DENGAN KONDISI FALSE
        if ((!berat_bener || !ekspedisi_bener || !tanggal_bener)|| !berat_bener && !ekspedisi_bener && !tanggal_bener) {
            if (berat_bener == false) {
                System.out.println("Berat tidak sesuai ketentuan!");
            }
            if (ekspedisi_bener == false) {
                System.out.println("Pilihan ekspedisi tidak valid!");
            }
            if (tanggal_bener == false) {
                System.out.println("Input tanggal tidak sesuai!");
            }
        }
        else if (berat_bener || ekspedisi_bener || tanggal_bener){

            System.out.println("Nama barang: " + nama);
            System.out.println("Tanggal sampai barang: " + tanggal + "/" + bulan + "/" + tahun);
            System.out.print("Biaya pengiriman: ");
            System.out.printf("Rp %,.2f", hargaPengiriman);
        }
    }
}