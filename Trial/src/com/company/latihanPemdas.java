package com.company;
// Nama = Nabillah Septianisa Nur Azizah
// NIM = 235150707111002

public class latihanPemdas {
//5.12 (Find the smallest n such that n^2 > 12,000)

    public static void main(String[] args) {
        // Operasi menemukan nilai terkecil dengan kondisi tertentu diawali baris ke-1
        // Maka find_n = 1 untuk menetapkan nilai awal variabel
        int find_n = 1;

        // Menggunakan operasi perulangan dengan kondisi bilangan berpangkat (Math.pow)
        while (Math.pow (find_n , 2) <= 12000) { // <= 12000 merupakan batas perulangan
            find_n++; // Pengulangan akan terus berlanjut hingga n mencapai lebih besar dari batas maka berhenti
        }
        // Nilai n yang berhenti terakhir itulah yang menjadi n terkecil
        System.out.println(find_n + " is the lowest number, such that n^2 is greater than 12,000");

        // Dibuktikan perbandingan antara n paling mendekati sebelum 12000
        System.out.println("Proof: " + (find_n - 1) + "^2 = " + Math.pow(find_n - 1, 2));
        // dan dibuktikan n paling mendekati setelah 12000
        System.out.println("Proof: " + find_n + "^2 = " + Math.pow(find_n, 2));
    }
}