package com.company;
import java.util.Scanner;

public class PraktikumCode{

    public static void main (String[]args){

        Scanner in = new Scanner(System.in);
        int in_baris, ulangi, out_bintang, spasi;

        System.out.print("Masukkan nilai n = ");
        in_baris = in.nextInt();
        System.out.println();

        for (ulangi=1; ulangi <= in_baris; ulangi++){
            for (spasi=in_baris; spasi>ulangi; spasi--) {
                System.out.print(" "+" "+" ");
            }
            for (out_bintang=1; out_bintang <= ulangi; out_bintang++){
                System.out.print("*"+" "+" ");
            }
            System.out.println();
        }
    in.close();
    }
}

