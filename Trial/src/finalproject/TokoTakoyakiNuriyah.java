package finalproject;
import java.util.Scanner;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/*
PROJECT AKHIR : PERHITUNGAN KASIR TOKO TAKOYAKI NURIYAH
NAMA          : Nabillah Septianisa Nur Azizah
NIM           : 235150707111002
KELAS         : TI - A
*/

public class TokoTakoyakiNuriyah {
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);

            // Format Real Time
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("        HH:mm:ss dd/MM/yyyy");
            LocalDateTime getNow = LocalDateTime.now();

            int pilih1 = 0, pilih2 = 0, pilih3 = 0;
            int hargaTakoyakiPentol = 12000;
            int hargaTakoyakiKeju = 15000;
            int hargaTakoyakiSosis = 10000;
            int hargaTakoyakiKepiting = 16000;
            int hargaTakoyakiLobster = 18000;
            int JumlahTakoyakiPentol, JumlahTakoyakiKeju, JumlahTakoyakiSosis, JumlahTakoyakiKepiting, JumlahTakoyakiLobster;
            int totalTakoyakiPentol = 0, totalTakoyakiKeju = 0, totalTakoyakiSosis = 0, totalTakoyakiKepiting = 0, totalTakoyakiLobster = 0;

            System.out.println("-------------------------------------Takoyaki Nuriyah-----------------------------------");
            System.out.println("                            Jl.Tjahaya Moeda no.33 Waru Sidoarjo ");
            System.out.println("                                  No.Telephone: 081231782345 ");
            System.out.println("========================================================================================");
            System.out.println("");
            System.out.println("                         Time and Date :" + dateTimeFormatter.format(getNow));
            System.out.println("\n");

            do {
                System.out.println("----------------------------------TAKOYAKI POINT OF SALE--------------------------------");
                System.out.println("1. Buy Takoyaki");
                System.out.println("2. Update Takoyaki Price");
                System.out.println("3. Owner Menu");
                System.out.println("4. Checkout");
                System.out.println("----------------------------------------------------------------------------------------");
                System.out.print("Enter your choice: ");
                pilih1 = sc.nextInt();
                System.out.println("========================================================================================");
                System.out.println();

                switch (pilih1) {
                    //Menu 1 : Buy Takoyaki
                    case 1 -> {
                        System.out.println("-----------------------------------MENU TAKOYAKI----------------------------------------");
                        do {
                            System.out.println("");
                            System.out.println("\n" +
                                               "1. Takoyaki Pentol (Harga awal)    -->               : Rp.12.000");
                            System.out.println("2. Takoyaki Keju  (Harga awal)     -->               : Rp.15.000");
                            System.out.println("3. Takoyaki Sosis (Harga awal)     -->               : Rp.10.000");
                            System.out.println("4. Takoyaki Kepiting (Harga awal)  -->               : Rp.16.000");
                            System.out.println("5. Takoyaki Lobster (Harga awal)   -->               : Rp.18.000");
                            System.out.println("6. Kembali");
                            System.out.println("----------------------------------------------------------------------------------------");
                            System.out.print("Pilih menu yang ingin di pesan = ");
                            pilih2 = sc.nextInt();
                            System.out.println("");

                            switch (pilih2) {
                                case 1 -> {
                                    System.out.println("Menu yang dipilih: ");
                                    System.out.println("1. Takoyaki Pentol");
                                    System.out.print(" Jumlah = ");
                                    JumlahTakoyakiPentol = sc.nextInt();
                                    totalTakoyakiPentol = JumlahTakoyakiPentol * hargaTakoyakiPentol;
                                    System.out.print(" Total = Rp." + totalTakoyakiPentol);
                                }
                                case 2 -> {
                                    System.out.println("Menu yang dipilih: ");
                                    System.out.println("2. Takoyaki Keju");
                                    System.out.print(" Jumlah = ");
                                    JumlahTakoyakiKeju = sc.nextInt();
                                    totalTakoyakiKeju = JumlahTakoyakiKeju * hargaTakoyakiKeju;
                                    System.out.print(" Total = Rp." + totalTakoyakiKeju);
                                }
                                case 3 -> {
                                    System.out.println("Menu yang dipilih: ");
                                    System.out.println("3. Takoyaki Sosis ");
                                    System.out.print(" Jumlah = ");
                                    JumlahTakoyakiSosis = sc.nextInt();
                                    totalTakoyakiSosis = JumlahTakoyakiSosis * hargaTakoyakiSosis;
                                    System.out.print(" Total = Rp." + totalTakoyakiSosis);
                                }
                                case 4 -> {
                                    System.out.println("Menu yang dipilih: ");
                                    System.out.println("4. Takoyaki Kepiting");
                                    System.out.print(" Jumlah = ");
                                    JumlahTakoyakiKepiting = sc.nextInt();
                                    totalTakoyakiKepiting = JumlahTakoyakiKepiting * hargaTakoyakiKepiting;
                                    System.out.print(" Total = Rp." + totalTakoyakiKepiting);
                                }
                                case 5 -> {
                                    System.out.println("Menu yang dipilih");
                                    System.out.println("5. Takoyaki Lobster");
                                    System.out.print(" Jumlah = ");
                                    JumlahTakoyakiLobster = sc.nextInt();
                                    totalTakoyakiLobster = JumlahTakoyakiLobster * hargaTakoyakiLobster;
                                    System.out.print(" Total = Rp." + totalTakoyakiLobster);
                                }
                                case 6 -> {
                                    System.out.println("========================================================================================");
                                    System.out.println("Grand total = Rp." + (totalTakoyakiPentol + totalTakoyakiKeju + totalTakoyakiSosis + totalTakoyakiKepiting + totalTakoyakiLobster));
                                    System.out.println();
                                }
                                default -> System.out.println("Maaf, Anda Memasukkan Opsi yang Tidak Pada Pilihan. Coba Lagi\n");
                            }
                        } while (pilih2 != 6);
                    }

                    //Menu 2 :Update Takoyaki Price
                    case 2 -> {
                        System.out.println("---------------------------------MENU UPDATE PRICE--------------------------------------");
                        System.out.println("");
                        System.out.println("<<<UBAH HARGA MENU>>>");
                        System.out.println("");
                        System.out.println("1. Takoyaki Pentol   ");
                        System.out.println("2. Takoyaki Keju     ");
                        System.out.println("3. Takoyaki Sosis    ");
                        System.out.println("4. Takoyaki Kepiting ");
                        System.out.println("5. Takoyaki Lobster  ");
                        System.out.println("----------------------------------------------------------------------------------------");
                        System.out.print("Pilih menu yang ingin diubah harganya : ");
                        pilih3 = sc.nextInt();
                        System.out.println("");

                        switch (pilih3) {
                            case 1 -> {
                                System.out.println("(Harga Takoyaki Pentol)");
                                System.out.println("Harga awal  : Rp." + hargaTakoyakiPentol);
                                System.out.print("\nHarga baru  : Rp.");
                                hargaTakoyakiPentol = sc.nextInt();
                            }
                            case 2 -> {
                                System.out.println("(Harga Takoyaki Keju)");
                                System.out.println("Harga awal  : Rp." + hargaTakoyakiKeju);
                                System.out.print("\nHarga baru  : Rp.");
                                hargaTakoyakiKeju = sc.nextInt();
                            }
                            case 3 -> {
                                System.out.println("(Harga Takoyaki Sosis)");
                                System.out.println("Harga awal  : Rp." + hargaTakoyakiSosis);
                                System.out.print("\nHarga baru  : Rp.");
                                hargaTakoyakiSosis = sc.nextInt();
                            }
                            case 4 -> {
                                System.out.println("(Harga Takoyaki Kepiting)");
                                System.out.println("Harga awal  : Rp." + hargaTakoyakiKepiting);
                                System.out.print("\nHarga baru  : Rp.");
                                hargaTakoyakiKepiting = sc.nextInt();
                            }
                            case 5 -> {
                                System.out.println("(Harga Takoyaki Lobster)");
                                System.out.println("Harga awal  : Rp." + hargaTakoyakiLobster);
                                System.out.print("\nHarga baru  : Rp.");
                                hargaTakoyakiLobster = sc.nextInt();
                            }
                            default -> System.out.println("Maaf, Anda Memasukkan Opsi yang Tidak Pada Pilihan. Coba Lagi\n");
                        }
                    }

                    //Menu 3 : Menu Owner
                    case 3 -> {
                        System.out.println("-----------------------------------MENU OWNER-------------------------------------------");
                        System.out.println("Total pemasukan harga Takoyaki Pentol     : Rp. " + totalTakoyakiPentol);
                        System.out.println("Total pemasukan harga Takoyaki Keju       : Rp. " + totalTakoyakiKeju);
                        System.out.println("Total pemasukan harga Takoyaki Sosis      : Rp. " + totalTakoyakiSosis);
                        System.out.println("Total pemasukan harga Takoyaki Kepiting   : Rp. " + totalTakoyakiKepiting);
                        System.out.println("Total pemasukan harga Takoyaki Lobster    : Rp. " + totalTakoyakiLobster);
                        System.out.println("========================================================================================");
                        System.out.println("");
                        System.out.println("GRAND TOTAL SEBESAR: Rp. " + (totalTakoyakiPentol + totalTakoyakiKeju + totalTakoyakiSosis + totalTakoyakiKepiting + totalTakoyakiLobster));
                        System.out.println("");
                    }

                    //Menu 4 : Checkout
                    case 4 -> {
                        System.out.println("TOTAL HARGA YANG HARUS DIBAYAR SEBESAR: Rp." + (totalTakoyakiPentol + totalTakoyakiKeju + totalTakoyakiSosis + totalTakoyakiKepiting + totalTakoyakiLobster));
                        System.out.println("");
                        System.out.println("------------------------------TERIMA KASIH, SILAHKAN DATANG KEMBALI---------------------");
                    }
                    default -> System.out.println("Maaf, Anda Memasukkan Opsi yang Tidak Pada Pilihan. Coba Lagi\n");
                }
            } while (pilih1 != 4);
           }
         }